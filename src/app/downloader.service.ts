import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }  from '@angular/common/http';
import { PlayerService } from './player.service';


import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from './rest.service';


import { File, FileEntry } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } 
        from '@ionic-native/file-transfer/ngx';



/*
  Maximum downloads or downloads limit is
  based on the size on storage not the number
  of tracks. If 10 tracks to be downloaded,
  avg size=20MB, then total size 200MB

*/
@Injectable({
  providedIn: 'root'
})
export class DownloaderService { 

  public audio: any;
  public message = '';
  data;
  public downloads:any[] = null;
  public progress;
  public progressText; 
  public downloading = false;
  nativeDir = "cdvfile://localhost/library-nosync/";


  constructor(
    	public http: HttpClient,
      public playerSrvs: PlayerService,
      public restSrvs: RestService,
      private storage: Storage,

      private ft: FileTransfer, 
      private file: File,
      private platform: Platform,
    	)
  {

    this.storage.get("downloads").then(val => {

      if (!val) {
        this.downloads = [];
        this.storage.set("downloads", this.downloads);
      } else {
        this.downloads = val;
      }

      // console.log(this.downloads);
      
    });

  } 



  isAlreadyDownloaded(track) {
    for (var i = 0; i < this.downloads.length; ++i) {
      if (this.downloads[i].id === track.id)
        return true;
    }

    return false;
  }



  public download(track, url) {

    this.downloading = true;
    this.progress = 0;
    this.progressText = 0;

    // this.message = url;


    if (this.isAlreadyDownloaded(track)) {
      this.message = "حملت مسبقا";
      return;
    }

    // let url = this.playerSrvs.generateUrl(track);

    console.log(url);

    this.message = "اعداد التحميل";

    let path = this.file.dataDirectory + "track" + track.id + ".mp3";
    const transfer = this.ft.create();

    this.message = "بداية التحميل";

    transfer.download(url, path)
    .then(entry => {
        this.message = "الانتهاء من التحميل";

        // change link to native url
        let nativePath = this.nativeDir + "track" + track.id + ".mp3";

        // deep copy so changing link doesn't 
        // effect original track object
        let newTrack = JSON.parse(JSON.stringify(track));

        newTrack.link = nativePath;

        this.downloads.push(newTrack);

        this.storage.set("downloads", this.downloads);
        this.restSrvs.createDownload(track.id);

    });


    transfer.onProgress((progress) => {
        this.message = "جاري التحميل";
        this.progress = progress.loaded / progress.total;
        this.progressText = Math.round((progress.loaded / progress.total) * 100);

    });

  }


  public remove(t) {

    console.log(this.downloads);

    this.storage.set("downloads", this.downloads);


    var dirPath = null;
    let filename = "track" + t.id + ".mp3";

    if (this.platform.is('android')) {
      dirPath = this.file.externalRootDirectory;
    } else if (this.platform.is('ios')) {
      dirPath = this.file.documentsDirectory;
    }

    this.file.removeFile(dirPath, filename);
  }




  public play(track) {
    this.playerSrvs.playTrack(track, track.link);
  }


}
