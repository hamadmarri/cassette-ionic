import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DownloadsPage } from './downloads.page';
import { SharedModule } from '../shared/shared.module'


const routes: Routes = [
  {
    path: '',
    component: DownloadsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

    SharedModule
  ],
  declarations: [DownloadsPage]
})
export class DownloadsPageModule {}
