import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';


@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.page.html',
  styleUrls: ['./downloads.page.scss'],
})
export class DownloadsPage implements OnInit {

	tracks: any;
	nextPage: any;


	constructor(
		public restSrvs: RestService,

		) { }

	ngOnInit() {
	}


	ionViewWillEnter() {
		this.getTracks();
	}


	getTracks(page = '') {  
		this.restSrvs.getDownloaded(page)
			.then(data => {
				if (this.tracks) {
	  				this.tracks.push(...data['results'].map(d => (d.track)));
				} else {
					this.tracks = data['results'].map(d => (d.track));
				}
	  			// this.previousPage = data['previous'];
	  			this.nextPage = data['next'];
	  			console.log(data);
			});
  	}


  	more(event) {
  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.getTracks(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  	}, 1200);
  	}
}
