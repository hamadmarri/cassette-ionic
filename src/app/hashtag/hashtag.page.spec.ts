import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HashtagPage } from './hashtag.page';

describe('HashtagPage', () => {
  let component: HashtagPage;
  let fixture: ComponentFixture<HashtagPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HashtagPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HashtagPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
