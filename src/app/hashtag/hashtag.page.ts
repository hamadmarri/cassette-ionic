import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from '../rest.service';
import { ToastController } from '@ionic/angular';
import { NavigatorService } from '../navigator.service';
import { IonContent } from '@ionic/angular';
import { PlayerService } from '../player.service';
import { PlaylistService } from '../playlist.service'
import { CachedService } from '../cached.service'


@Component({
  selector: 'app-hashtag',
  templateUrl: './hashtag.page.html',
  styleUrls: ['./hashtag.page.scss'],
})
export class HashtagPage implements OnInit {

  	@ViewChild("hashContent") hashContent: IonContent;


	tracks: any;
	nextPage: any;
	hashtag: any;
	playlistForPlayer = null;
  	
  	constructor(
  		public restSrvs: RestService,
  		public toastController: ToastController,
  		public navigatorSrvs: NavigatorService,
    	public playerSrvs: PlayerService,
		private playlistSrvs: PlaylistService,
		public cachedSrvs: CachedService,
  		
  		) { }


  	ngOnInit() { }


  	ionViewWillEnter() {
  		this.update(this.navigatorSrvs.page_data().get('h'), '');
  	}


  	ionViewDidEnter() {
  		this.hashContent.scrollToTop();	 
  	}


  	update(hashtag, page = '') {
  		this.hashtag = hashtag;

  		if (page == '')
  			this.tracks = [];

		this.restSrvs.searchAllTracks(this.hashtag.name, page)
			.then(data => {
				if (this.tracks) {
	  				this.tracks.push(...data['results']);
				} else {
					this.tracks = data['results'];
				}
	  			this.nextPage = data['next'];
			});
  	}



  	more(event) {

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.update(this.hashtag, this.nextPage);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  	}, 1200);
  		
  	}


	playTrack(track) {
  		this.playerSrvs.playTrack(track);

  		// deep copy
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: this.hashtag.name,
			tracks: tracks,
			next: this.nextPage,
			moreTracks: 'searchAllTracks',
			hashtag_id: this.hashtag.id,
			random: 'getRandomTrackOfHashtag'
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
  	}


  	faveHashtag() {
		this.restSrvs.faveHashtag(this.hashtag.id)
			.then(res => {
	  			this.presentToast(this.hashtag.name + " اضيفت الى المفضلة");
			});
	}


	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}

}
