import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ToastController } from '@ionic/angular';
import { NavigatorService } from '../navigator.service';
import { PlayerService } from '../player.service';
import { DownloaderService } from '../downloader.service';
import { AdsService } from '../ads.service';


@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

	song_name = '';
	artist_name = '';
	song_url = ''; //https://cdn1.esm3.com//music/4558/m277904.mp3';
	remarks = ''; 
	all_text = '';
	error = '';
	isForLestien = false;
	isLestining = false;

	rewardDoneSub;
	canDownload = false;


	constructor(
		private restSrvs: RestService,
		public toastController: ToastController,
  		private navigatorSrvs: NavigatorService,
  		private playerSrvs: PlayerService,
		public downloaderSrvs: DownloaderService,
    	public adsSrvs: AdsService,
		) {

			this.rewardDoneSub = this.adsSrvs.rewardDone.subscribe(() => {
				this.canDownload = true;
			});
		}



	ngOnInit() {
	}




  	ngOnDestroy() {
  		this.rewardDoneSub.unsubscribe();
  	}



  	ionViewWillEnter() {
		this.adsSrvs.showReward();
  	}



	getAllText() {
		this.all_text = this.song_name + '*' 
			+ this.artist_name + '*' 
			+ this.song_url + '*' + this.remarks;

		return this.all_text;
	}


	isValid() {
		this.getAllText();

		if (this.all_text.length < 9 || this.all_text.length > 500)
			return false;

		if (this.song_url.indexOf(".exe") != -1)
			return false;


		if (this.song_url.substr(-4) === ".mp3")
			this.isForLestien = true;
		else
			this.isForLestien = false;


		return true;
	}


	send() {

		this.restSrvs.createFeedback(this.getAllText())
		.then(() => {
			this.presentToast("شكرا لك, سيتم اضافة الاغنية قريبا");
			this.navigatorSrvs.back();
		}).catch(e => {
			this.error = e.error.detail;
			// console.log(e.error.detail);
		});
	}



	makeid(length) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
		  result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}

		console.log('random id:', result);
		return result;
	}


	lesten() {
		if (this.song_url.substr(-4) === ".mp3") {
			console.log("mp3");
			let track = {
				id: this.makeid(10),
				name: this.song_name,
				artists: [
				{
					name: this.artist_name,
				}
				],
			};

			this.playerSrvs.playTrack(track, '', this.song_url);


			this.checkIfIsLestining(4, 500);
		}
	}


	checkIfIsLestining(attempts, time) {
		this.isLestining = false;

		if (attempts === 0) {
			return;
		}

		setTimeout(() => {
			if (this.playerSrvs.currentBuffer > 0) {
				this.isLestining = true;
			} else {
				this.checkIfIsLestining(attempts - 1, time * 2);
			}
		}, time)
	}



	download() {
		this.downloaderSrvs.download(this.playerSrvs.track, this.song_url);
	}



	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 4000
		});
		toast.present();
	}

}
