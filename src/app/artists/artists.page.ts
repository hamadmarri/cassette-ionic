import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';


@Component({
  selector: 'app-artists',
  templateUrl: './artists.page.html',
  styleUrls: ['./artists.page.scss'],
})
export class ArtistsPage implements OnInit {
	
	artists:any[] = [];
	next:any = null;
	artists_count = -1;
	tracks_count = -1;

	constructor(
		public restSrvs: RestService,
		) { }

	ngOnInit() {
	}


	ionViewDidEnter() {

		if (this.artists.length === 0)
			this.getArtists();
	}


	getArtists(page = '') {
		this.restSrvs.getArtists(page)
		.then(data => {
			this.artists.push(...data['results']);
			this.next = data['next'];
			this.artists_count = data['count'];
		});

		this.restSrvs.getTracks('')
		.then(data => {
			this.tracks_count = data['count'];
		});
	}
 

	more(event) {

	  	setTimeout(() => {
	  		if (this.next) {
	  			this.getArtists(this.next);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  	}, 1200);
	}
}
