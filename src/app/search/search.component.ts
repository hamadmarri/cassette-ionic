import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { interval } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { CachedService } from '../cached.service';

@Component({
  selector: 'app-search',
  inputs:['searchText', 'enabledSearch'],
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {


	tracks: any;
	artists: any;
	hashtags: any;
	usernames: any;

	tracks_count: number = 0;
	artists_count: number = 0;
	hashtags_count: number = 0;
	usernames_count: number = 0;

	enabledSearch: boolean;


	searchText = '';
	prevText = '';
	isSearchDone = true;
	
	previousPage: any;
	nextPage: any;
	updateSearchInterval = interval(2000);
	// isNoResults = true;
	isAll = true;
	isTracks = false;
	isArtists = false;
	isHashtags = false;
	isUsernames = false;

	numberOfSearch: number = 0;

	addingPlaylist = false;
	newPlaylistName = '';

	typed = false;


	constructor(
		public restSrvs: RestService,
		public toastController: ToastController,
		public cachedSrvs: CachedService,
		) {
		
		this.updateSearch();
	}

	ngOnInit() {}


	changeFilter(filter = 1) {

		this.typed = false;

		// all
		if (filter == 1) {
			this.isAll = true;
			this.isTracks = false;
			this.isArtists = false;
			this.isHashtags = false;
			this.isUsernames = false;
		} 
		// tracks
		else if (filter == 2) {
			this.isAll = false;
			this.isTracks = true;
			this.isArtists = false;
			this.isHashtags = false;
			this.isUsernames = false;
		}
		// artists
		else if (filter == 3) {
			this.isAll = false;
			this.isTracks = false;
			this.isArtists = true;
			this.isHashtags = false;
			this.isUsernames = false;
		}
		// hashtags
		else if (filter == 4) {
			this.isAll = false;
			this.isTracks = false;
			this.isArtists = false;
			this.isHashtags = true;
			this.isUsernames = false;
		}
		// usernames
		else if (filter == 5) {
			this.isAll = false;
			this.isTracks = false;
			this.isArtists = false;
			this.isHashtags = false;
			this.isUsernames = true;
		}


		// do search again
		
		// clear all search results
		this.clearSearchResults();	


		// set prev = current
		this.prevText = this.searchText;

		// set search to not done
		this.isSearchDone = false;
	}


	updateSearch() {

		// update search
		this.updateSearchInterval.subscribe(x => {
			// if prev = current, and search not done
			if ((this.prevText === this.searchText)
					&& this.isSearchDone == false
					&& this.searchText != '') {
				
				// do search. set search done to true
				this.doSearch();
				
			} else if (this.prevText != this.searchText) {
				// if prev != current 
				
				// set search to not done
				this.isSearchDone = false;

				// set prev = current
				this.prevText = this.searchText;

				this.typed = true;

				// clear all search results
				this.clearSearchResults();
			}
			
		});

	}


	doSearch() {

		console.log("do search");

		if (this.enabledSearch) {

			this.tracks_count = 0;
			this.artists_count = 0;
			this.hashtags_count = 0;
			this.usernames_count = 0;

			if (this.searchText.length < 3) {
				this.setSearchDone();
				return;
			}


			// hide keyboard
			let elm = document.getElementById('searchInput');
			elm.querySelector("input").blur();
			

			this.numberOfSearch = 0;

			if (this.isAll) {
				this.numberOfSearch++;
				this.searchAllTracks();
			} else if (this.isTracks) {
				this.numberOfSearch++;
				this.searchTracks();
			}

			
			if (this.isAll || this.isArtists) {
				this.numberOfSearch++;
				this.searchArtists();
			}

			if (this.isAll || this.isHashtags) {
				this.numberOfSearch++;
				this.searchHashtags();
			}

			if (this.isAll || this.isUsernames) {
				if (this.cachedSrvs.user_details) {
					this.numberOfSearch++;
					this.searchUsernames();
				}
			}

		} 
	}


	clearSearchResults() {
		if (this.tracks)
			this.tracks.length = 0;
		if (this.artists)
			this.artists.length = 0;
		if (this.hashtags)
			this.hashtags.length = 0;
		if (this.usernames)
			this.usernames.length = 0;
	}



	setSearchDone() {
		this.numberOfSearch--;

		if (this.numberOfSearch === 0) {
			this.isSearchDone = true;
			console.log("search done");
		}
	}



	addFeedback() {
		if (this.typed === false)
			return;

		if (this.isAll || this.isTracks) {
			this.restSrvs.createFeedback("Search track: " 
				+ this.searchText);
		} else if (this.isArtists) {
			this.restSrvs.createFeedback("Search artist: " 
				+ this.searchText);
		} else if (this.isHashtags) {
			this.restSrvs.createFeedback("Search hashtag: " 
				+ this.searchText);
		}
	}


	searchAllTracks(page = '') { 

		if (this.enabledSearch) {
			this.restSrvs.searchAllTracks(this.searchText, page)
				.then(data => {
					if (this.tracks) {
		  				this.tracks.push(...data['results']);
					} else {
						this.tracks = data['results'];
					}
		  			this.previousPage = data['previous'];
		  			this.nextPage = data['next'];
		  			this.tracks_count = data['count'];
		  			console.log("searchAllTracks", this.tracks_count);

		  			if (this.tracks_count === 0) 
		  				this.addFeedback();

		  			// console.log(this.tracks_count);
				}).finally(() => {
		  			this.setSearchDone();
				});	
		}
  	}



	searchTracks(page = '') { 

		if (this.enabledSearch) {
			this.restSrvs.searchTracks(this.searchText, page)
				.then(data => {
					if (this.tracks) {
		  				this.tracks.push(...data['results']);
					} else {
						this.tracks = data['results'];
					}
		  			this.previousPage = data['previous'];
		  			this.nextPage = data['next'];

		  			if (this.tracks.length == 0) {
		  				this.searchAllTracks();
		  			} else {
		  				this.tracks_count = data['count'];
		  				console.log("searchTracks", this.tracks_count);
		  				this.setSearchDone();
		  			}
		  			
		  			// console.log(this.tracks_count);
		  			
				}).catch(e => {
					this.setSearchDone();
				});	
		}
  	}


  	moreSearchTracks(event) {
  		// console.log("moreSearchTracks is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.searchTracks(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			// event.target.disabled = true;
	  			event.target.complete();
	  		}
	  	}, 1200);
  	}



  	searchArtists(page = '') { 

		if (this.enabledSearch) {
			this.restSrvs.searchArtists(this.searchText, page)
				.then(data => {
					if (this.artists) {
		  				this.artists.push(...data['results']);
					} else {
						this.artists = data['results'];
					}
		  			this.previousPage = data['previous'];
		  			this.nextPage = data['next'];					
		  			this.artists_count = data['count'];
		  			
		  			if (!this.isAll && this.artists_count === 0)
		  				this.addFeedback();

		  			// console.log(data);
				}).finally(() => {
		  			this.setSearchDone();
				});	
		}
  	}


  	moreSearchArtists(event) {
  		// console.log("moreSearchArtists is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.searchArtists(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			// event.target.disabled = true;
	  			event.target.complete();
	  		}
	  	}, 1200);
  	}


	searchHashtags(page = '') { 

		if (this.enabledSearch) {
			this.restSrvs.searchHashtags(this.searchText, page)
				.then(data => {
					if (this.hashtags) {
		  				this.hashtags.push(...data['results']);
					} else {
						this.hashtags = data['results'];
					}
		  			this.previousPage = data['previous'];
		  			this.nextPage = data['next'];					
		  			this.hashtags_count = data['count'];

		  			if (!this.isAll && this.hashtags_count === 0)
		  				this.addFeedback();

		  			// console.log(data);
				}).finally(() => {
		  			this.setSearchDone();
				});	
		}
  	}


  	moreSearchHashtags(event) {
  		// console.log("moreSearchHashtags is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.searchHashtags(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			// event.target.disabled = true;
	  			event.target.complete();
	  		}
	  	}, 1200);
  	}



  	searchUsernames(page = '') { 

		if (this.enabledSearch) {
			this.restSrvs.searchUsernames(this.searchText, page)
				.then(data => {
					if (this.usernames) {
		  				this.usernames.push(...data['results']);
					} else {
						this.usernames = data['results'];
					}
		  			this.previousPage = data['previous'];
		  			this.nextPage = data['next'];					
		  			this.usernames_count = data['count'];

		  			// console.log(data);
				}).finally(() => {
		  			this.setSearchDone();
				});	
		}
  	}


  	moreSearchUsernames(event) {
  		// console.log("moreSearchUsernames is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.searchUsernames(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			// event.target.disabled = true;
	  			event.target.complete();
	  		}
	  	}, 1200);
  	}



  	showAddPlaylist() {
  		this.newPlaylistName = this.searchText;
		this.addingPlaylist = true;
	}



	addPlaylist() {

		this.restSrvs.createPlaylist(this.newPlaylistName)
		.then(res => {
						
			this.presentToast("تم اضافة " + this.newPlaylistName);
			this.newPlaylistName = '';
			this.addingPlaylist = false;
			this.doSearch();
		});
	}


	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
