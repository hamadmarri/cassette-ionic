import { Injectable, EventEmitter, Output } from '@angular/core';
import { RestService } from './rest.service'
import { interval } from 'rxjs';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MusicControls } from '@ionic-native/music-controls/ngx';
import { CachedService } from './cached.service';
import { AdsService } from './ads.service';



@Injectable({
  providedIn: 'root'
})
export class PlayerService {

	@Output() audioEnded = new EventEmitter();

	public audio: any = null;
  	private audioMedia = null;

	public track: any = null;
	public original_song: any = null;
	public cover_songs: any = null;
	public trackDetails: any = null;
	public trackComments: any = null;
	public nextPage: string = '1';
	public duration = '0:00';
	currentTime = '0:00';
	public currentPosition = 0;
	currentBuffer = 0;

	addedListen = false;
	updatePositionhInterval = interval(1000);
	updatePositionhIntervalSub: any;

	public status = "pause"; 

	isDownloaded = false;
	isFetched = false;

	loading_status = '';

	minimini = false;

	ads_counter = 0;


	constructor(
		private restSrvs: RestService,
  		public cachedSrvs: CachedService,
      	private media: Media,
      	private musicControls: MusicControls,
    	public adsSrvs: AdsService,
		) {}
 

	playTrack(track, nativeUrl='', fetchUrl='') {

		// check if the track is playing so close it
		if (this.audio || this.audioMedia) {

			// but first check if to play the same track 
			// then exit do nothing
			if(this.track == track) {
				return;
			}

			this.closeTrack();
		}


		this.duration = '0:00';
		this.currentTime = '0:00';


		if (!nativeUrl) {

			if (fetchUrl) {
				this.isFetched = true;
				this.audio = new Audio();
				this.audio.src = fetchUrl;
				this.currentBuffer = 0;
				this.addAudioListeners();
			} else {
				this.isFetched = false;
				this.getAudioFromCache(track);
			}


	    	this.isDownloaded = false;
	    	
	    	this.audio.play();
		    
		    this.audio.addEventListener("ended", () => {
				this.audioEnded.emit(null);
			});
		}
	    else if (nativeUrl) {
			this.audioMedia = this.media.create(nativeUrl);

			this.audioMedia.onStatusUpdate
			.subscribe(status => {
				 // 4 == Media.MEDIA_STOPPED 
				if (status === 4 && this.currentPosition > 0.95) {
					this.audioEnded.emit(null);
				}
			});

			this.audioMedia.play({ playAudioWhenScreenIsLocked : true });

			this.currentBuffer = 1;
			this.audioMedia.setVolume('1.0');
	    	this.isDownloaded = true;
			// this.setupMusicControls();

	    }


		this.track = track; 
	    this.status = "pause";
	   
	    this.updatePosition();

	    this.addedListen = false;


	    this.showAds();
  	}



  	showAds() {
		if (this.ads_counter === 0) {
	    	this.ads_counter++;

	    	// if (!this.isDownloaded)
	    	// 	this.audio.play();
	    	// else
	    	// 	this.audioMedia.play({ playAudioWhenScreenIsLocked : true });

		} else if (this.ads_counter < 4) {
			// show ads Interstitial
			this.adsSrvs.showInterstitial();
	    	this.ads_counter++;
	    	// this.status = "pause";
		} else if (this.ads_counter === 4) {
			// show ads reward
			this.adsSrvs.showReward();
	    	this.ads_counter = 1;
	    	// this.status = "pause";
		}
  	}
 



  	getAudioFromCache(track) {
  		let cache_k = "playerSrvs:" + track.id;
  		this.currentBuffer = 0;

  		if (!this.cachedSrvs.has(cache_k)) {
  			console.log("!cached", cache_k);

  			let audio = new Audio();
	    	audio.src = this.generateUrl(track);

	    	// do cache
			// this.cachedSrvs.cache(cache_k, audio);
			this.cachedSrvs.cache(cache_k, audio);
   		 
  			this.audio = audio;
  			// this.audio.currentTime = 0;
			this.addAudioListeners();
  		} else {

	  		// if cached already
	  		let cache_v = this.cachedSrvs.get(cache_k);
		  	cache_v.currentTime = 0;
		  	this.currentBuffer = 1;

	  		console.log("cached", cache_k, cache_v);

	  		this.audio = cache_v;
  		}
  	}
 

  	generateUrl(track) {
  		let artist_name = track.artists[0].name;
  		let track_name = track.name;
  		let url = this.restSrvs.getStreamUrl() + "/" + artist_name
  				+ "/" + track_name + ".mp3";

  		return encodeURI(url);
  	}


  	addAudioListeners() {

  		this.audio.addEventListener("progress", () => {
  			if (this.audio) {
				for(var i = 0; i < this.audio.buffered.length; i ++)
				  {
				    this.currentBuffer = this.audio.buffered.end(i);
				    this.currentBuffer /= Math.round(this.audio.duration);
				  }

				  // console.log(this.audio);
  			}

		});
  	}


  	public pause_play() {
  		if (!this.isDownloaded) {
			if (this.status == "pause"){
				this.audio.pause();
				this.status = "play";
			}
			else {
				this.audio.play();
				this.status = "pause";
			}
  		} else {
  			if (this.status == "pause"){
				this.audioMedia.pause();
				this.status = "play";
			}
			else {
				this.audioMedia.play({ playAudioWhenScreenIsLocked : true });
				this.status = "pause";
			}
  		}
	}


  	public play_again() {
  		if (!this.isDownloaded) {
	  		this.audio.currentTime = 0;
		    this.addedListen = false;
	  		this.audio.play();
  		} else {
  			this.audioMedia.seekTo(0);
		    this.addedListen = false;
	  		this.audioMedia.play();
  		}
  	}



  	seekTo(pos) {
  		
  		if (!this.isDownloaded) {
  			let newTime = this.audio.duration * pos;
  			this.audio.currentTime = newTime;
  		}
  		else {
  			let newTime = this.audioMedia.getDuration() * pos;
  			console.log("seekTo", newTime * 1000);
  			this.audioMedia.seekTo(newTime * 1000);
  		}
  	}

  

  	closeTrack() {
  		if (this.audio) {
	  		this.audio.pause();
	    	this.audio = null;
	    	this.updatePositionhIntervalSub.unsubscribe();
	    	this.currentPosition = 0;
  		} else if (this.audioMedia) {
			this.musicControls.destroy();
  			this.audioMedia.stop();
  			this.audioMedia.release();
  			this.audioMedia = null;
	    	this.updatePositionhIntervalSub.unsubscribe();
	    	this.currentPosition = 0;
  		}
  	}



  	updatePosition() { 
  		this.updatePositionhIntervalSub = 
  			this.updatePositionhInterval.subscribe(x => {

  				var duration;
		  		var current;

  				if (!this.isDownloaded) {
		  			duration = Math.round(this.audio.duration);
			  		current = Math.round(this.audio.currentTime);

			  		if (duration) {
			  			this.currentPosition = (current * 1.0) / duration;

			  			this.convertToTime(current, duration);
			  		
				  		// when reach %30, add listening +1
				  		if (!this.isFetched && !this.addedListen 
				  				&& current / duration >= 0.3) {
							this.restSrvs.listen(this.track.id);
				  			this.addedListen = true;
				  		}
			  		}
  				} else {
					duration = Math.round(this.audioMedia.getDuration());
					this.audioMedia.getCurrentPosition(pos => {
		  				current = Math.round(pos);

		  				// console.log(current + ", " + duration);

				  		if (duration) {
				  			this.currentPosition = (current * 1.0) / duration;
				  			this.convertToTime(current, duration);
				  		}
					});
  				}
		  		
	  		});
  	}


  	convertToTime(current, duration) {
  		if (current) {
	  		this.currentTime = Math.round((current / 60)).toString().padStart(2, '0') + ":";
	  		this.currentTime += (current % 60).toString().padStart(2, '0');

	  		this.duration = Math.round((duration / 60)).toString().padStart(2, '0') + ":";
	  		this.duration += (duration % 60).toString().padStart(2, '0');
  		}
  	}


  	resetDetails() {
		// reset comments
	    this.resetComments();


	    // reset original and coverts
	    this.original_song = null;

	    if (this.cover_songs)
	    	this.cover_songs.length = 0;
	    
	    this.cover_songs = null;
  	}


  	resetComments() {
		if (this.trackComments)
			this.trackComments.length = 0;

		this.trackComments = null;
		this.nextPage = '1';
  	}


  	fetchAllDetails() {
		this.fetchTrackDetails();
		
		if(this.track.original_song)
			this.getOriginalSong();

		this.getCoverSongs();
  	}

  	

  	fetchTrackDetails() {

  		if (this.track) {
			this.restSrvs.getTrackDeatails(this.track.trackdetails)
			.then(data => {
				this.trackDetails = data;
	  			// console.log(data);
			});
  		}
	}



	getTrackComments(page = '') {  
		this.restSrvs.getTrackComments(this.track.id, page)
			.then(data => {
				if (this.trackComments) {
	  				this.trackComments.push(...data['results']);
				} else {
					this.trackComments = data['results'];
				}
	  			this.nextPage = data['next'];
	  			// console.log(data);
			});
  	}



  	moreComments(event) {
  		// console.log("more is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			if(this.nextPage === '1') {
	  				this.getTrackComments();
	  			} else {
	  				this.getTrackComments(this.nextPage);
	  			}
	  			event.target.complete();
	  		} else {
	  			event.target.complete();
	  		}
	  	}, 1200);
  	}



  	getOriginalSong() {
  		this.restSrvs.getTrack(this.track.original_song)
			.then(data => {
				this.original_song = data;
	  			console.log(data);
			});
  	}



  	getCoverSongs(page = '') {
		this.restSrvs.getCoverSongs(this.track.id, page)
			.then(data => {
				if (this.cover_songs) {
	  				this.cover_songs.push(...data['results']);
				} else {
					this.cover_songs = data['results'];
				}

				// console.log(this.cover_songs);

				if (data['next']) {
	  				this.getCoverSongs(data['next']);
				}

			});
  	}



  	setupMusicControls() {

		this.musicControls.create({
		  track       : this.track.name,        // optional, default : ''
		  artist      : this.track.artists[0].name,                       // optional, default : ''
		  cover       : this.track.artists[0].file,      // optional, default : nothing
		  // cover can be a local path (use fullpath 'file:///storage/emulated/...', or only 'my_image.jpg' if my_image.jpg is in the www folder of your app)
		  //           or a remote url ('http://...', 'https://...', 'ftp://...')
		  isPlaying   : true,                         // optional, default : true
		  dismissable : true,                         // optional, default : false

		  // hide previous/next/close buttons:
		  hasPrev   : false,      // show previous button, optional, default: true
		  hasNext   : false,      // show next button, optional, default: true
		  hasClose  : true,       // show close button, optional, default: false

		// iOS only, optional
		  // album       : 'Absolution',     // optional, default: ''
		  // duration : this.duration, // optional, default: 0
		  // elapsed : 10, // optional, default: 0
		  hasSkipForward : true,  // show skip forward button, optional, default: false
		  hasSkipBackward : true, // show skip backward button, optional, default: false
		  skipForwardInterval: 15, // display number for skip forward, optional, default: 0
		  skipBackwardInterval: 15, // display number for skip backward, optional, default: 0
		  hasScrubbing: true, // enable scrubbing from control center and lockscreen progress bar, optional

		  // Android only, optional
		  // text displayed in the status bar when the notification (and the ticker) are updated, optional
		  // ticker    : 'Now playing "Time is Running Out"',
		  // // All icons default to their built-in android equivalents
		  // playIcon: 'media_play',
		  // pauseIcon: 'media_pause',
		  // prevIcon: 'media_prev',
		  // nextIcon: 'media_next',
		  // closeIcon: 'media_close',
		  // notificationIcon: 'notification'
		 });

		 this.musicControls.subscribe().subscribe(action => {

		   function events(action) {
		     const message = JSON.parse(action).message;
		         switch(message) {
		             case 'music-controls-next':
		                 // Do something
		                 break;
		             case 'music-controls-previous':
		                 // Do something
		                 break;
		             case 'music-controls-pause':
		                 // Do something
		                 break;
		             case 'music-controls-play':
		                 // Do something
		                 break;
		             case 'music-controls-destroy':
		                 // Do something
		                 break;

		         // External controls (iOS only)
		         case 'music-controls-toggle-play-pause' :
		                 // Do something
		                 break;
		         case 'music-controls-seek-to':
		           const seekToInSeconds = JSON.parse(action).position;
		           this.musicControls.updateElapsed({
		             elapsed: seekToInSeconds,
		             isPlaying: true
		           });
		           // Do something
		           break;
		         case 'music-controls-skip-forward':
		           // Do something
		           break;
		         case 'music-controls-skip-backward':
		           // Do something
		           break;

		             // Headset events (Android only)
		             // All media button events are listed below
		             case 'music-controls-media-button' :
		                 // Do something
		                 break;
		             case 'music-controls-headset-unplugged':
		                 // Do something
		                 break;
		             case 'music-controls-headset-plugged':
		                 // Do something
		                 break;
		             default:
		                 break;
		         }
		     }

		 this.musicControls.listen(); // activates the observable above

		 this.musicControls.updateIsPlaying(true);




		});
	}


} // end class

// source;
// analyser;
// isAlreadyShown = false;

// audioContext: AudioContext = new (window["AudioContext"] || window["webkitAudioContext"])();
// audioContext: AudioContext = null;

// if (!this.audioContext)
// 	this.audioContext = new (window["AudioContext"] || window["webkitAudioContext"])();


// this.audio.crossOrigin = 'anonymous';

// streamURL = this.restSrvs.apiUrl + "/stream/";
// streamURL = "https://drive.google.com/uc?export=download&id=";


// this.audio.load();

// this.prepareAnalyzer(); 

// this.isAlreadyShown = false;


// 	prepareAnalyzer() { 

// if (!this.isAlreadyShown) {

// 	this.analyser = this.audioContext.createAnalyser();	
// 	this.analyser.fftSize = 64;
// 	this.analyser.smoothingTimeConstant = 0.38;
	
// 	this.source = this.audioContext.
// 		createMediaElementSource(this.audio);

// 	// this.source.connect(this.audioContext.destination);
// 	this.source.connect(this.analyser);
// 	this.analyser.connect(this.audioContext.destination);


//  		this.isAlreadyShown = true;
// }
// 	}

// this.audioContext.close();
