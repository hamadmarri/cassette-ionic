import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders, HttpParams }  from '@angular/common/http';
import { RestService } from './rest.service';
import { CachedService } from './cached.service';
import { NavigatorService } from './navigator.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

	private isLoggedIn = false;
	

  	constructor(
  		private router: Router,
  		private http: HttpClient,
  		private restSrvs: RestService,
  		private cacheSrvs: CachedService,
		private storage: Storage,
		private navigatorSrvs: NavigatorService,
		) {
    }



    logout() {

    	this.storage.remove("token").then(() => {
    		this.storage.remove("userdetails").then(() => {
		    	this.isLoggedIn = false;
		    	this.restSrvs.token = '';
		    	this.navigatorSrvs.goTo('login');
    		});
    	});

    }


    canActivate(route: ActivatedRouteSnapshot): Promise<boolean>|boolean {

		// this.storage.clear();
 
    	if (this.isLoggedIn && this.restSrvs.token != '') {
    		return true;
    	} else {
    		return this.check();
    	}
        
    }


    private check(): Promise<boolean> {

    	// load token from storage
    	return this.storage.get('token').then(token => {
			// console.log('storage: token', token);

			// if not empty, set rest.token, 
			// islogged to true in and return true
			if (token) {
				this.isLoggedIn = true;
				// this.restSrvs.token = 'Token ' + token;
				this.restSrvs.setToken('Token ' + token);
				return true;
			}
        	// if empty load user user credentials from storage
			else {
				// return this.getUsercredentials();
				this.router.navigate(['login']);
				return false;
			}

			console.error("AuthService: done check()");
		});

    }



	private getToken(usercredentials) {
		console.log('AuthService.getToken(): getting token');

		return new Promise((resolve, reject) => {
		this.http.post(this.restSrvs.apiUrl+'/api-token-auth/',
			JSON.stringify(usercredentials),
			{
				headers: 
				new HttpHeaders(
					{
						'Content-Type': 'application/json'
					}
				)	
				}).subscribe(data => {
					resolve(data);
			}, err => {
	  			console.error("getToken:", err);
	  			reject(err);
			});
		});
	}


	public login(usercredentials): Promise<any> {
		console.log('login');

		return new Promise((resolve, reject) => {
			this.getToken(usercredentials)
				// set token and isLoggedIn
				.then(data => {
					console.log("AuthService.login: done getToken()", data);

					this.isLoggedIn = true;
					this.restSrvs.setToken('Token ' + data['token']);

					// store token
					this.storage.set('token', data['token']);

					// store usercredintials
					// this.storage.set('usercredentials', usercredentials);

					// update user details
					this.updateUserDetails();

					resolve(true);
				})
				// if incorrect return false
				.catch(function(e){
					console.log("incorrect user credentials");
	            	reject(e);
				});
		});
	}


	public login_guest(): Promise<any> {
		console.log('login_guest');

		let usercredentials = { 
			username: 'guest', 
			password: 'guest1234'
		};

		return new Promise((resolve, reject) => {
			this.getToken(usercredentials)
				// set token and isLoggedIn
				.then(data => {
					console.log("done getToken()", data);

					this.isLoggedIn = true;
					this.restSrvs.setToken('Token ' + data['token']);

					// uncache user details
					this.cacheSrvs.user_details_set(null);

					resolve(true);
				})
				// if incorrect return false
				.catch(function(e){
					console.log("incorrect user credentials");
	            	reject(e);
				});
		});
	}


	updateUserDetails() {
		this.restSrvs.getUserDetails().then(data => {
			// console.log(data['results'][0]);
			let userdetails = data['results'][0];
			// store usercredintials
			this.storage.set('userdetails', userdetails);

			// cache user details
			this.cacheSrvs.user_details_set(userdetails);
		});
	}
}
