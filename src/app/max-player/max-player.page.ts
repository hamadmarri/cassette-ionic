import { Component, NgZone,
	OnInit, ViewChild, ElementRef
	 } from '@angular/core';

import { PlayerService } from '../player.service';
import { PlaylistService } from '../playlist.service';
import { RestService } from '../rest.service';
import { ToastController } from '@ionic/angular';
import { interval } from 'rxjs';
import { IonContent } from '@ionic/angular';
import { CachedService } from '../cached.service';
import { NavigatorService } from '../navigator.service';
import { DownloaderService } from '../downloader.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';


@Component({
  selector: 'app-max-player',
  templateUrl: './max-player.page.html',
  styleUrls: ['./max-player.page.scss'],
})
export class MaxPlayerPage implements OnInit {

  	// @ViewChild("visAudio") visAudio: ElementRef;
  	@ViewChild("maxContent") maxContent: IonContent;


  	public context; //: CanvasRenderingContext2D;

  	w;
	h;
	wScale;
	// freqByteData;

  	// drawInterval = interval(56); // 80 
	// drawIntervalSub: any;
	isComenting = false;
	newCommentText = '';

	constructor(
		public playerSrvs: PlayerService,
		public playlistSrvs: PlaylistService,
		public restSrvs: RestService,
		public toastController: ToastController,
		public ngZone: NgZone,
		public cachedSrvs: CachedService,
		public navigatorSrvs: NavigatorService,
		public downloaderSrvs: DownloaderService,
		private socialSharing: SocialSharing,
		) {

	}
 
  	ngOnInit() {
  		this.playlistSrvs.isMaxOpened = true;
  	}


  	ionViewWillEnter() {

  		this.downloaderSrvs.downloading = false;

		// this.visAudio.nativeElement.width = window.innerWidth * 0.8;
		// this.visAudio.nativeElement.height = window.innerWidth * 0.8 * 0.24;

		// this.context = (<HTMLCanvasElement>this.visAudio.nativeElement)
		// 	.getContext('2d');

		// this.context.fillStyle = '#488aff';

		// this.w = (<HTMLCanvasElement>this.visAudio.nativeElement).width;
		// this.h = (<HTMLCanvasElement>this.visAudio.nativeElement).height;
		// this.wScale = this.w / (32 * 1.5); 


		// this.freqByteData = new Uint8Array(this.playerSrvs.analyser.frequencyBinCount);

		// this.drawIntervalSub = 
		// 	this.drawInterval.subscribe(x => {
				
		// 		this.ngZone.runOutsideAngular(() => {
		// 	      requestAnimationFrame(() => {
		// 	      	this.drawBars();
		// 	      });
		//     });

		// }); 


  	}

  	ionViewDidEnter() {
  		this.maxContent.scrollToTop(); 
  	}

  	ngOnDestroy() {
  		// this.drawIntervalSub.unsubscribe();
  		this.playlistSrvs.isMaxOpened = false;
  	}


  // 	drawBars() {
  // 		this.playerSrvs.analyser.getByteFrequencyData(this.freqByteData);
		// this.context.clearRect(0, 0, this.w, this.h);
		

		// // console.log("freqByteData.length", this.freqByteData.length);

		// // freqByteData.length (=) 1024

		// var j = 0;
		// for (var i = 1; i < this.freqByteData.length; i++) {
		// 	// console.log(freqByteData[i], ((freqByteData[i]) / 255) * h);

		// 	if (this.freqByteData[i] > 5) {
  //       		this.context.fillRect(j + 1, this.h - (((this.freqByteData[i]) / 255) * this.h), this.wScale, this.h);
		// 		// this.context.strokeRect(i + 2, h - freqByteData[i] * 1.5, 2, h);
		// 	}
			
		// 	j += this.wScale + 5;
		// } 
  // 	}
 


  	pause_play() {
		this.playerSrvs.pause_play();
	}


	close() { 
		
		this.playerSrvs.closeTrack();
		this.playerSrvs.track = null;

		if (this.playerSrvs.trackComments)
			this.playerSrvs.trackComments.length = 0;

		this.playerSrvs.trackComments = null;

		this.navigatorSrvs.back();
	}


	seekTo(e) {
		var x = e.offsetX;
		// var width = e.target.offsetWidth;
		var width = document.body.offsetWidth;
		let pos = x / width;

    	// console.log(x, width, pos);
		this.playerSrvs.seekTo(pos);
	}


	playTrack(track) {
		console.log(track);

		this.playerSrvs.playTrack(track);
		this.playerSrvs.resetDetails();
  		this.playerSrvs.fetchAllDetails();
	}


	play_next() {
		this.playlistSrvs.play_next();
	}


  	play_previous() {
		this.playlistSrvs.play_previous();
  	}

	addToFavorite() {
		this.restSrvs.addToFavorite(this.playerSrvs.track.id)
			.then(res => {
	  			this.presentToast(this.playerSrvs.track.name + " اضيفت الى المفضلة");
			});
	}


	bookmarkTrack() {
		this.restSrvs.bookmarkTrack(this.playerSrvs.track.id)
			.then(res => {
	  			this.presentToast("تم حفظ " + this.playerSrvs.track.name);
			});
	}


	addToPlaylist() {
		let u = this.cachedSrvs.user_details;

	  	this.restSrvs.getKasaitUser(u.id).then(data => {
	  		
	  		let map = new Map([
	  			['u', data],
	  			['t', this.playerSrvs.track]
	  		]);

			this.navigatorSrvs.goTo('add-to-playlist', map);
	  	});
	}


	downloadTrack() {
		// let trackUrl = this.playerSrvs.track.file;
		// let streamURL = this.restSrvs.apiUrl + "/stream/";
		// let url = streamURL + trackUrl.substr(-36);

		let url = this.playerSrvs.generateUrl(this.playerSrvs.track);
		this.downloaderSrvs.download(this.playerSrvs.track, url);
	}


	addComment() {
		this.restSrvs.addComment(this.playerSrvs.track.id, 
			this.newCommentText)
		.then(() => {
			this.newCommentText = '';
			this.playerSrvs.resetComments();
			this.playerSrvs.getTrackComments();
			this.isComenting = false;
		});
	}
	

	deleteComment(c) {
		this.restSrvs.deleteComment(c.id)
		.then(() => {
			this.playerSrvs.resetComments();
			this.playerSrvs.getTrackComments();
		});
	}


	showArtist(a) {
	    this.navigatorSrvs.goTo("artist", new Map([['a', a]]));
	}


	// showHashtag(h) {
	// 	this.navigatorSrvs.goTo("hashtag", new Map([['h', h]])); 
	// }


	share() {

		this.socialSharing.shareViaWhatsApp(
			'استمع الى اغنية ' + this.playerSrvs.track.name + " من خلال تطبيق (كاسيت)",
			 null /* img */,
			  // 'itms-apps://itunes.com/apps/hamad.s.almarri@gmail.com'
			  'itms-apps://itunes.apple.com/app/id1461425223'
			  )
		.then(() => {
			console.log('share ok');
		 })
		.catch((e) => {
		  console.log(e);
		});
			   
	}


	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}

}
