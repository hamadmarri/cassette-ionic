import { OnInit, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerService } from '../../player.service';
import { PlaylistService } from '../../playlist.service';
import { RestService } from '../../rest.service';
import { CachedService } from '../../cached.service';
import { NavigatorService } from '../../navigator.service';


import { DownloaderService } from '../../downloader.service';


@Component({
  selector: 'app-track-feed',
  inputs: ['track', 'tracks', 'artists', 'hashtags', 'users', 'albums',],
  templateUrl: './track-feed.component.html',
  styleUrls: ['./track-feed.component.scss'],
})
export class TrackFeedComponent implements OnInit {


  track: any;
  tracks: any;
  artists: any;
  hashtags: any;
  users: any;
  albums: any;


  constructor(
    public playerSrvs: PlayerService,
    public playlistSrvs: PlaylistService,
    public navigatorSrvs: NavigatorService,
    public restSrvs: RestService,
    public cachedSrvs: CachedService,
    ) { }


  ngOnInit() {}
  


  playTrack(track) {
    
    
  	this.playerSrvs.playTrack(track);

    // deep copy
    let tracks = JSON.parse(JSON.stringify(this.tracks));

    let playlistForPlayer = {
      playlist_id: null,
      tracks: tracks,
      next: null,
      moreTracks: null,
      random: 'search',
    };

    this.playlistSrvs.playlist_set(playlistForPlayer);
  }
  


  showHashtag(h) {
    if (this.cachedSrvs.user_details && h.playlist) {
      this.restSrvs.getPlaylist(h.playlist)
      .then(data => {

        let p = data;

        // this.cachedSrvs.playlist_set(p);

        this.restSrvs.getKasaitUser(p['kasaituser'])
        .then(data => {

          let map = new Map([ 
            ['u', data],
            ['p', p],
          ]);

          this.navigatorSrvs.goTo('playlist', map);

        });
      });

     

    } else {
      this.navigatorSrvs.goTo("hashtag", new Map([['h', h]]));
    }

  }

  showUserProfile(u) {
    this.navigatorSrvs.goTo("user-profile", new Map([['u', u]]));
  }

  showArtist(a) {
    this.navigatorSrvs.goTo("artist", new Map([['a', a]]));
  }

  showAlbum(a) {
    this.navigatorSrvs.goTo("album", new Map([['a', a]]));
  }

}
