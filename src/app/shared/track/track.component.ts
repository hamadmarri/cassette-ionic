import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-track',
  inputs:['track',],
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
})
export class TrackComponent implements OnInit {

	track: any;

	constructor() { }

	ngOnInit() {}

}
