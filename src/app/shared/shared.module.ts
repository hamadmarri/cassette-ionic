import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { TrackFeedComponent } from '../shared/track-feed/track-feed.component';
import { MiniPlayerComponent } from '../shared/mini-player/mini-player.component';
import { WelcomeComponent } from '../shared/welcome/welcome.component';
import { TrackComponent } from '../shared/track/track.component';



@NgModule({
  declarations: [
  	TrackFeedComponent,
    MiniPlayerComponent,
    WelcomeComponent,
    TrackComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    TrackFeedComponent,
    MiniPlayerComponent,
    WelcomeComponent,
    TrackComponent,
  ]
})
export class SharedModule { }
