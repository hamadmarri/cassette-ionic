import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../player.service';
import { PlaylistService } from '../../playlist.service';
import { ToastController } from '@ionic/angular';
import { RestService } from '../../rest.service';
import { NavigatorService } from '../../navigator.service';


@Component({
  selector: 'app-mini-player',
  templateUrl: './mini-player.component.html',
  styleUrls: ['./mini-player.component.scss'],
})
export class MiniPlayerComponent implements OnInit {



	constructor(
		public playerSrvs: PlayerService,
		public playlistSrvs: PlaylistService,
		public navigatorSrvs: NavigatorService,
		public toastController: ToastController,
		public restSrvs: RestService,

		) {
	}
 
	ngOnInit() {
	} 



	showMinimini() {
		this.playerSrvs.minimini = true;
	}


	hideMinimini() {
		this.playerSrvs.minimini = false;
	}



	pause_play() {
		this.playerSrvs.pause_play();
	}


	close() { 
		this.playerSrvs.closeTrack();
		this.playerSrvs.track = null;
		this.playlistSrvs.playlist_set(null);
	}



	seekTo(e) {
		var x = e.offsetX;
		// var width = e.target.offsetWidth;
		var width = document.body.offsetWidth;
		let pos = x / width;

    	// console.log(x, width, pos);
		this.playerSrvs.seekTo(pos);
	}


	showMaxPlayer() {
  		this.playerSrvs.resetDetails();
  		this.playerSrvs.fetchAllDetails();
  		// this.playerSrvs.prepareAnalyzer();
  		this.navigatorSrvs.goTo("max-player");
	}


	showArtist(a) {
	    this.navigatorSrvs.goTo("artist", new Map([['a', a]]));
	}


	addToFavorite() {
		this.restSrvs.addToFavorite(this.playerSrvs.track.id)
			.then(res => {
	  			this.presentToast(this.playerSrvs.track.name + " اضيفت الى المفضلة");
			});
	}
  	

	play_next() {
		this.playlistSrvs.play_next();
	}


  	play_previous() {

  		if (this.playerSrvs.currentPosition < 0.01) {
			this.playlistSrvs.play_previous();  			
  		} else {
  			this.playerSrvs.play_again();
  		}

  	}
  	

  	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
