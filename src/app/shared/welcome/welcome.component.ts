import { Component, OnInit } from '@angular/core';
import { CachedService } from '../../cached.service'
import { RestService } from '../../rest.service'
import { NavigatorService } from '../../navigator.service';



@Component({
  selector: 'app-welcome',
  inputs: ['back'],
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {


  back = false;

  constructor(
  	public cachedSrvs: CachedService,
  	public restSrvs: RestService,
    public navigatorSrvs: NavigatorService,
  	) { }

  ngOnInit() {}
 

  showMyProfile() {
  	let u = this.cachedSrvs.user_details;

  	this.restSrvs.getKasaitUser(u.id).then(data => {

      let map = new Map([
        ['u', data],
        ['o', true]
       ]);

    	this.navigatorSrvs.goTo("user-profile", map);
  	});
  }


  goHome() {
    this.navigatorSrvs.goHome();  
  }


  goBack() {    
    this.navigatorSrvs.back();
  }


  search(event) {
    event.preventDefault();
    event.stopPropagation();
    this.navigatorSrvs.goHome(true);
  }


}
