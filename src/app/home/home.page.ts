import { Component } from '@angular/core';
import { AppComponent } from '../app.component';
import { RestService } from '../rest.service';
import { PlayerService } from '../player.service';
import { DownloaderService } from '../downloader.service';
import { PlaylistService } from '../playlist.service'
import { NavigatorService } from '../navigator.service';
import { AdsService } from '../ads.service'; 


enum Pages {
	Home,
	Search,
	Hashtag,
	Playlist,
	MaxPlayer,
}


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	public pages = Pages;

	tracks: any;
	nextPage: any;
	fontSize = 50;
	fontSizePx = this.fontSize + 'px';
	page = [true, false, false, false, false];
	searchText = ''; 
	noConnectivity = false;


	constructor(
		public appCmp: AppComponent,
		public restSrvs: RestService,
		public playerSrvs: PlayerService,
		public downloaderSrvs: DownloaderService,
		private playlistSrvs: PlaylistService,
		private navigatorSrvs: NavigatorService,
    	public adsSrvs: AdsService,
		) {



			appCmp.showMyDownloadsEvent.subscribe(() => {
				this.showDownloads();
			});


			navigatorSrvs.showSearch.subscribe(() => {

				setTimeout(() => {
					console.log("show search");

					// show keyboard
					let elm = document.getElementById('searchInput');
					elm.querySelector("input").focus();
					elm.querySelector("input").click();

				}, 500);
			});


			setTimeout(() => {
		      this.adsSrvs.showBanner();
		    }, 3000);
	}



	ionViewWillEnter() {
		if (!this.tracks || this.tracks.length === 0) {
			this.tracks = [];
			this.getTracks();
		}

		this.noConnectivity = false;
	}


	setPage(page, show_hide: boolean) {
		this.page = [false, false, false, false, false];
		this.page[page] = show_hide;

		// console.log(this.page);
	}


	clearSearch() {
		var searchInput = <HTMLInputElement>document.getElementById("searchInput");
		searchInput.value = '';
		this.searchText = '';
		this.setPage(Pages.Home, true);
		this.fontSize = 50;
		this.fontSizePx = this.fontSize + 'px';
	}

	resizeSearch(elmnt) {

		if ((elmnt.scrollWidth > elmnt.clientWidth)
		 		&& this.fontSize > 25) {

            this.fontSize *= elmnt.clientWidth / (elmnt.scrollWidth * 1.3);
			this.fontSizePx = this.fontSize + 'px';
        } 
        else if (elmnt.value == '') {
            this.fontSize = 50;
			this.fontSizePx = this.fontSize + 'px';
        }

        this.searchText = elmnt.value;
        
        if (elmnt.value != '') {
        	if (this.page[Pages.Search] === false)
        		this.setPage(Pages.Search, true);
        }
    	else
        	this.setPage(Pages.Home, true);

	}


	showDownloads() {
		setTimeout(() => {
			if(!this.downloaderSrvs.downloads) {
				this.showDownloads();
			} else {
				this.tracks = this.downloaderSrvs.downloads;
				this.noConnectivity = true;
			}

		}, 1000);
	}
 

 
	getTracks(page = '') { 

		this.restSrvs.getTracks(page)
			.then(data => {
				if (this.tracks) {
	  				this.tracks.push(...data['results']);
				} else {
					this.tracks = data['results'];
				}
	  			// this.previousPage = data['previous'];
	  			this.nextPage = data['next'];
	  			// console.log(data);
			}).catch(err => {
				console.error(err); 

				this.showDownloads();
			});
  	}


  	more(event) {
  		console.log("more is called");

  		if (this.noConnectivity) {
  			event.target.complete();
  			return;
  		}

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.getTracks(this.nextPage);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  	}, 1200);
  	}



  	playTrack(track) {
		this.downloaderSrvs.play(track);

		// deep copy
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		let playlistForPlayer = {
			playlist_id: null,
			tracks: tracks,
			next: null,
			moreTracks: null,
			random: 'getRandomDownloaded',
			download: true,
		};

		this.playlistSrvs.playlist_set(playlistForPlayer);


	}


	remove(t) {

		let index = this.tracks.findIndex((el) => {
			return (el.id === t.id);
		});
		
		this.tracks.splice(index, 1);
		// console.log(this.hashtags);

		this.downloaderSrvs.remove(t); 
  	}

}
