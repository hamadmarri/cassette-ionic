import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams }  from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {


  // apiUrl = 'http://192.168.8.171:8000/kasait';
  // apiUrl = 'http://192.168.8.171/kasait';
  apiUrl = 'http://165.22.67.206/kasait';
	// apiUrl = 'http://noconnectivity-test';

  streamUrls;

	givenUrl = '';
	token = '';



  constructor(public http: HttpClient) {
  }


  setToken(token) {
    this.token = token;


    this.getStreamSites().then(data => {
      let sites = data['results'];

      // if free version
        this.streamUrls = sites.filter(el => {
          return el.type == 0;
        });
      // else

      console.log(this.streamUrls);
    });

  }


  
  getStreamUrl() {
    // for multiple servers do balancer

    // for one server aka current implimentation

    // console.log(this.streamUrls[0].url);
    return this.streamUrls[0].url;
  }


  getStreamSites() {
    this.givenUrl = this.apiUrl + '/streamsites/';
    return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }



  getUserDetails() {
    this.givenUrl = this.apiUrl + '/userdetails/';
    return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }



  getArtists(page = '') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/artists/';
    } else {
      this.givenUrl = page;  
    } 

      return new Promise((resolve, reject) => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              reject(err);
          });
      });
  }


	getTracks(page = '') {
		if (page == '') {
			this.givenUrl = this.apiUrl + '/tracks/';
		} else {
			this.givenUrl = page;	
		} 

  		return new Promise((resolve, reject) => {
    		this.http.get(this.givenUrl,
    			{
    				headers: 
    				new HttpHeaders(
    					{
    						'Authorization': this.token,
							'Content-Type': 'application/json'
    					}
    				)

  				}).subscribe(data => {
      				resolve(data);
	    		}, err => {
	      			reject(err);
	    		});
  		});
	}

  getTrack(id=0) {
    
      this.givenUrl = this.apiUrl + '/tracks/' + id + "/";
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  getTracksOfPlaylist(id=0, page='') {
    
    if (page == '') {
      this.givenUrl = this.apiUrl + '/playlists/' + id + "/tracks/";
    } else {
      this.givenUrl = page;  
    }
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  getRandomTracksOfPlaylist(id=0) {
    
    this.givenUrl = this.apiUrl + '/playlists/' + id + "/random/";

    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  getKasaitUser(id=0) {
    
    this.givenUrl = this.apiUrl + '/kasaitusers/' + id + "/";
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  getHashtag(id=0) {
    
      this.givenUrl = this.apiUrl + '/hashtags/' + id + "/";
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


    getRandomTrackOfHashtag(id=0) {
    
      this.givenUrl = this.apiUrl + '/hashtags/' + id + "/random/";
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  getCoverSongs(id=0, page='') {
    
    if (page == '') {
      this.givenUrl = this.apiUrl + '/coversongs/' + id + "/";
    } else {
      this.givenUrl = page;  
    }
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  searchTracks(searchText='', page='') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/tracks/?search=' + searchText;
    } else {
      this.givenUrl = page;  
    }
    

      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  searchAllTracks(searchText='', page='') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/search/?search=' + searchText;
    } else {
      this.givenUrl = page;  
    }

      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  searchArtists(searchText='', page='') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/artists/?search=' + searchText;
    } else {
      this.givenUrl = page;  
    }

      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  searchHashtags(searchText='', page='') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/hashtags/?search=' + searchText;
    } else {
      this.givenUrl = page;  
    }

      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }



  searchUsernames(searchText='', page='') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/kasaitusers/?search=' + searchText;
    } else {
      this.givenUrl = page;  
    }

      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


	// addUser(data) {
 //    return new Promise((resolve, reject) => {
 //    		this.http.post(this.apiUrl+'/tracks', JSON.stringify(data),
 //    			{
 //    				headers: new HttpHeaders().set('Authorization', this.token),
 //    				params: new HttpParams().set('id', '3'),
 //  				})
 //      		.subscribe(res => {
 //        		resolve(res);
 //      		}, (err) => {
 //        		reject(err);
 //      		});
 //  		});
	// }



  getTrackDeatails(id = 1) {

    this.givenUrl = this.apiUrl + '/trackdetails/' + id + '/';

    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }

  getTrackComments(id = 1, page='') {

    if (page == '') {
      this.givenUrl = this.apiUrl + '/tracks/' + id + '/comments/';
    } else {
      this.givenUrl = page;  
    }


    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });

  }


  addComment(tid=0, text='') {
    let data = {
      "text":text,
      "track":tid,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/createcomment/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  deleteComment(id=0) {
    let data = {
      "comment":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/deletecomment/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  addToFavorite(id=0) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/addtofavorite/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }



  removeFromFavorite(id=0) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/removefromfavorite/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }



  getFaveSongs(uid=0, page='') {
    
    if (page == '') {
      this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/favoritesongs/";
    } else {
      this.givenUrl = page;  
    }
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  getRandomFaveSong(uid=0) {
    
    this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/favoritesongs/random/";

    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  bookmarkTrack(id=0) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/bookmarktrack/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  unbookmarkTrack(id=0) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/unbookmarktrack/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


    getBookmarkedSongs(uid=0, page='') {
    
    if (page == '') {
      this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/bookmarkedsongs/";
    } else {
      this.givenUrl = page;  
    }
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  getRandomBookmarkedSong(uid=0) {
    
    this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/bookmarkedsongs/random/";
    
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }

  faveHashtag(id=0) {
    let data = {
      "hashtag":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/favehashtag/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  unfaveHashtag(id=0) {
    let data = {
      "hashtag":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/unfavehashtag/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }



  getFaveHashtags(uid=0, page='') {
    
    if (page == '') {
      this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/favoritehashtags/";
    } else {
      this.givenUrl = page;  
    }
    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });
  }


  getPlaylist(id=0) {
    
      this.givenUrl = this.apiUrl + '/playlists/' + id + "/";
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  getPlaylistsOf(id=0, page='') {

    if (page == '') {
      this.givenUrl = this.apiUrl + '/playlists/?search=' + id;
    } else {
      this.givenUrl = page;  
    }


    return new Promise(resolve => {
      this.http.get(this.givenUrl,
        {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
            'Content-Type': 'application/json'
            }
          )

        }).subscribe(data => {
            resolve(data);
        }, err => {
            console.log(err);
        });
    });

  }


  addToPlaylist(pid=0, tid=0) {
    let data = {
      "playlist":pid,
      "track":tid,
    }

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/addtracktoplaylist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  removeFromPlaylist(pid=0, tid=0) {
    let data = {
      "playlist":pid,
      "track":tid,
    }

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/removetrackfromplaylist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  getFollowers(uid=0, page='') {
    
      if (page == '') {
        this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/followers/";
      } else {
        this.givenUrl = page;  
      }

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  getFollowingUsers(uid=0, page='') {

      if (page == '') {
        this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/followingusers/";
      } else {
        this.givenUrl = page;  
      }

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  getFollowingArtists(uid=0, page='') {

      if (page == '') {
        this.givenUrl = this.apiUrl + '/kasaitusers/' + uid + "/followingartists/";
      } else {
        this.givenUrl = page;  
      }     
    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }



  createPlaylist(name) {
    let data = {
      "name":name,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/createplaylist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


 deletePlaylist(id) {
    let data = {
      "playlist":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/deleteplaylist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }



  subscribeKasaitUser(id) {
    let data = {
      "other_kasait_user":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/subscribekasaituser/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  unsubscribeKasaitUser(id) {
    let data = {
      "other_kasait_user":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/unsubscribekasaituser/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  subscribeArtist(id) {
    let data = {
      "artist":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/subscribeartist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }


  unsubscribeArtist(id) {
    let data = {
      "artist":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/unsubscribeartist/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }

  

  getTracksOfArtist(id=0, page='') {

      if (page == '') {
        this.givenUrl = this.apiUrl + '/artists/' + id + "/tracks/";
      } else {
        this.givenUrl = page;  
      }

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }


  getRandomTracksOfArtist(id=0) {


    this.givenUrl = this.apiUrl + '/artists/' + id + "/tracks/random/";

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  getAlbumsOfArtist(id=0, page='') {

      if (page == '') {
        this.givenUrl = this.apiUrl + '/albumsofartist/' + id + "/";
      } else {
        this.givenUrl = page;  
      }

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }

  getTracksOfAlbum(id=0, page='') {

      if (page == '') {
        this.givenUrl = this.apiUrl + '/tracksofalbum/' + id + "/";
      } else {
        this.givenUrl = page;  
      }

    
      return new Promise(resolve => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              console.error(err);
          });
      });
  }




  listen(id) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/listen/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }





  getDownloaded(page = '') {
    if (page == '') {
      this.givenUrl = this.apiUrl + '/downloaded/';
    } else {
      this.givenUrl = page;  
    } 

      return new Promise((resolve, reject) => {
        this.http.get(this.givenUrl,
          {
            headers: 
            new HttpHeaders(
              {
                'Authorization': this.token,
              'Content-Type': 'application/json'
              }
            )

          }).subscribe(data => {
              resolve(data);
          }, err => {
              reject(err);
          });
      });
  }



  createDownload(id) {
    let data = {
      "track":id,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/createdownload/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders( 
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }






  signupKasaituser(username='', password='', lastname='', firstname='',
        bio='', gender='0', avatar) {

    let formData = new FormData();

      formData.append("bio", bio);
      formData.append("gender", gender);

      if (avatar) {
        console.log("avatar");
        formData.append("avatar", avatar, username + '.jpeg');
      }

      formData.append("username", username);
      formData.append("lastname", lastname);
      formData.append("firstname", firstname);
      formData.append("password", password);


    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/kasaitusers/',
          formData,  
          // JSON.stringify(data),
          {
          headers: 
          new HttpHeaders(
            {
              // 'Authorization': this.token,
              // 'Content-Type': 'application/json'
              // 'Content-Type': 'multipart/form-data'
              // 'Content-Type': 'application/x-www-form-urlencoded'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }





  createFeedback(text) {
    let data = {
      "text":text,
    }
          

    return new Promise((resolve, reject) => {
        this.http.post(this.apiUrl+'/createfeedback/', 
          JSON.stringify(data),
          {
          headers: 
          new HttpHeaders( 
            {
              'Authorization': this.token,
              'Content-Type': 'application/json'
            }
          )
          })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
  }

}
