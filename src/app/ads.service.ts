import { Injectable, Output, EventEmitter } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';


@Injectable({
  providedIn: 'root'
})
export class AdsService {


	@Output() rewardDone = new EventEmitter();


	constructor(
    	private admobFree: AdMobFree,
  	) { }


    showBanner() {

	    // this.admobFree.banner.config({
	    //   // add your config here
	    //   // for the sake of this example we will just use the test config
	    //	 IOS
	    //   id: 'ca-app-pub-3504979136002068/9649109482',
	    //	 ANDROID
	    //	 id: 'ca-app-pub-3504979136002068/7725066997',
	    //   // isTesting: true,
	    //   autoShow: true,
	    //   // size: 'LARGE_BANNER',
	    //   // bannerAtTop: true,
	    //   // offsetTopBar: true,
	    //   overlap: false,
	    // });

	    // this.admobFree.banner.prepare().then(() => {
	    // }).catch(e => console.log(e));

	}



	showInterstitial() {
		// this.admobFree.interstitial.config({
	    //	IOS
		// 	id: 'ca-app-pub-3504979136002068/7378169367',
		//	 ANDROID
	    //	 id: 'ca-app-pub-3504979136002068/2608471689',
		// 	// isTesting: true,
		// 	autoShow: true
		// });



		// setTimeout(() => {
		// 	this.admobFree.interstitial.prepare().then(() => {
		// 	}).catch(e => console.log(e));
		// }, 2000);
		
	}



 
	showReward() {
		// this.admobFree.rewardVideo.config({
	    //	IOS
		// 	id: 'ca-app-pub-3504979136002068/6650221682',
		//	 ANDROID
	    //	 id: 'ca-app-pub-3504979136002068/2921486392',
		// 	// isTesting: true,
		// 	autoShow: true
		// });


		// this.admobFree.rewardVideo.prepare().then(() => {
		// 	}).catch(e => console.log(e));


		// document.addEventListener('admob.rewardvideo.events.REWARD', (event) => {
		// 	this.rewardDone.emit();
	 //    });

	}

}
