import { Component, OnInit } from '@angular/core';
import { CachedService } from '../cached.service';
import { NavigatorService } from '../navigator.service';
import { RestService } from '../rest.service';
import { ToastController } from '@ionic/angular';
import { PlayerService } from '../player.service';
import { PlaylistService } from '../playlist.service'


@Component({
  selector: 'app-artist',
  templateUrl: './artist.page.html',
  styleUrls: ['./artist.page.scss'],
})
export class ArtistPage implements OnInit {

	artist = null;
	_isSubscribed = -1;
	tracks:any[] = [];
	nextTracks = null;

	albums:any[] = [];
	nextAlbums = null;
	playlistForPlayer = null;


	constructor(
		public cachedSrvs: CachedService,
		public navigatorSrvs: NavigatorService,
		private restSrvs: RestService,
		public toastController: ToastController,
    	public playerSrvs: PlayerService,
		private playlistSrvs: PlaylistService,
		) { }


	ngOnInit() { }


	ionViewWillEnter() {
	    // console.log("ionViewWillEnter");
		this.artist = this.navigatorSrvs.page_data().get('a');

	    this.tracks = [];
	    this.albums = [];
	    this.getTracks();
		this.getAlbums();
	}	


	getTracks(page='') {
		// let a = this.cachedSrvs.artist_get();
		

		this.restSrvs.getTracksOfArtist(this.artist.id, page)
		.then(data => {
			this.tracks.push(...data['results']);
			this.nextTracks = data['next'];
		});
	}


	moreTracks() {
  		this.getTracks(this.nextTracks);
	}


	playTrack(track) {
		// console.log(track);
		this.playerSrvs.playTrack(track);


		// deep copy
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: this.artist.id,
			tracks: tracks,
			next: this.nextTracks,
			moreTracks: 'getTracksOfArtist',
			random: 'getRandomTracksOfArtist'
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
	}


	getAlbums(page='') {
		// let a = this.cachedSrvs.artist_get();

		this.restSrvs.getAlbumsOfArtist(this.artist.id, page)
		.then(data => {
			this.albums.push(...data['results']);
			this.nextAlbums = data['next'];
		});
	}



	isSubscribed() {

		if (this._isSubscribed > -1) {
			return (this._isSubscribed === 1);
		}

		// let a = this.cachedSrvs.artist_get();


		let index = this.cachedSrvs.following_artists
					.findIndex(el => {
						return el === this.artist.id
					});

		if (index > -1)
			this._isSubscribed = 1;
		else
			this._isSubscribed = 0;

		return (this._isSubscribed === 1);
	}


	subscribe() {
		// let a = this.cachedSrvs.artist_get();

		this.restSrvs.subscribeArtist(this.artist.id)
		.then(res => {
			
			this.cachedSrvs.cacheFollowings();
			this._isSubscribed = 1;
			this.artist.subscribe_count++;

			this.presentToast("تمت متابعة " 
				+ this.artist.name);
		});
	}


	unsubscribe() {
		// let a = this.cachedSrvs.artist_get();

		this.restSrvs.unsubscribeArtist(this.artist.id)
		.then(res => {
			
			this.cachedSrvs.cacheFollowings();
			this._isSubscribed = 0;
			this.artist.subscribe_count--;
			
			this.presentToast("تم الغاء متابعة " 
				+ this.artist.name);
		});
	}



	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
