import { Injectable } from '@angular/core';
import { RestService } from './rest.service'
import { PlayerService } from './player.service'



enum Modes {
	OFF,
	REPEATE_ONCE,
	REPEATE_LIST,
	RANDOM,
}


@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

	isListplay = false;

	modes = Modes;
	mode;


	public playlist:any = null;
	public index_in_playlist;
	timer = null;

	isMaxOpened = false;



  constructor(
  	public restSrvs:RestService,
  	public playerSrvs:PlayerService,

  	) {

  		this.mode = this.modes.OFF;

  		this.playerSrvs.audioEnded.subscribe(() => {
  			// console.log("ended");

  			this.play_next();
  		});
  	}


	public playlist_set(playlist) {
		this.playlist = playlist;
		this.index_in_playlist = 0;
		this.isListplay = true;

		if (this.mode == this.modes.OFF)
			this.mode = this.modes.REPEATE_LIST;
	}


	getMoreTracks(page='') {
  		let pid = this.playlist.playlist_id;
  		let tracks = this.playlist.tracks;
 
 		switch (this.playlist.moreTracks) {
 			case "getTracksOfPlaylist":
 				this.restSrvs.getTracksOfPlaylist(pid, page)
				.then(data => {
					tracks.push(...data['results']);
			  		this.playlist.next = data['next'];
			  		this.index_in_playlist--;			  		
			  		this.play_next();
				});
 				break;

 			case "getFaveSongs":
 				this.restSrvs.getFaveSongs(pid, page)
				.then(data => {
					tracks.push(...data['results']);
			  		this.playlist.next = data['next'];
			  		this.index_in_playlist--;
			  		this.play_next();
				});
 				break;

 			case "getBookmarkedSongs":
 				this.restSrvs.getBookmarkedSongs(pid, page)
				.then(data => {
					tracks.push(...data['results']);
			  		this.playlist.next = data['next'];
			  		this.index_in_playlist--;
			  		this.play_next();
				});
 				break;

 			case "searchAllTracks":
 				this.restSrvs.searchAllTracks(pid, page)
				.then(data => {
					tracks.push(...data['results']);
			  		this.playlist.next = data['next'];
			  		this.index_in_playlist--;
			  		this.play_next();
				});
 				break;

 			case "getTracksOfArtist":
 				this.restSrvs.getTracksOfArtist(pid, page)
				.then(data => {
					tracks.push(...data['results']);
			  		this.playlist.next = data['next'];
			  		this.index_in_playlist--;
			  		this.play_next();
				});
 				break;
 				
 		}
	}

 
  	play_next() {

  		if (this.isOFF())
  			return;
  		
  		
  		if (this.isRepeatOnce()) {
  			this.play_again();
  			return;
  		}


  		if (!this.playlist)
  			return;


  		if (this.isRandom()) {
  			this.play_random();
  			return;
  		}

  		if (this.index_in_playlist === 0) {
  			this.index_in_playlist 
				= this.playlist.tracks.findIndex(el => {
						return el.id == this.playerSrvs.track.id;
					});
  		}
 

		if (this.timer)
			clearTimeout(this.timer);
  		
  		this.index_in_playlist++;
  		// console.log(this.index_in_playlist);


  		this.timer = setTimeout(() => {
  			// console.log(this.index_in_playlist);

	  		if (this.index_in_playlist >= this.playlist.tracks.length) {
	  			if (this.playlist.next) {
	  				this.getMoreTracks(this.playlist.next);
	  			} else {
	  				this.index_in_playlist = 0;
			  		let nextTrack = this.playlist.tracks[this.index_in_playlist];
			  		this.play(nextTrack);
	  			}
	  		} else {
	  			let nextTrack = this.playlist.tracks[this.index_in_playlist];
		  		this.play(nextTrack);
	  		}

  		}, 2000);

  	}


  	play(track) {
  		if (this.playlist.download){
  			this.playerSrvs.closeTrack();
			this.playerSrvs.playTrack(track, track.link);
  		}
		else {
			this.playerSrvs.playTrack(track);

			if (this.isMaxOpened) {
				this.playerSrvs.resetDetails();
  				this.playerSrvs.fetchAllDetails();
			}
		}
  	}


	play_previous() {
		if (!this.playlist)
  			return;

  		if (this.index_in_playlist === 0) {
  			this.index_in_playlist 
				= this.playlist.tracks.findIndex(el => {
						return el.id == this.playerSrvs.track.id;
					});
  		}
 

		if (this.timer)
			clearTimeout(this.timer);
  		
  		this.index_in_playlist--;
  		// console.log(this.index_in_playlist);


  		this.timer = setTimeout(() => {
  			// console.log(this.index_in_playlist);

	  		if (this.index_in_playlist < 0) {
  				this.index_in_playlist = 0;
		  	}

  			let nextTrack = this.playlist.tracks[this.index_in_playlist];
	  		this.play(nextTrack);  		
	  		

  		}, 800);
  	}  	


  	play_again() {
	  	this.playerSrvs.play_again();
  	}


  	play_random() {
  		let pid = this.playlist.playlist_id;
 
 		switch (this.playlist.random) {
 			case "getRandomTracksOfPlaylist":
 				this.restSrvs.getRandomTracksOfPlaylist(pid)
				.then(data => {

					if (!data['detail']) {
						let track = data['results'][0];
						this.play(track);
					}
				});
 				break;
 			case "getRandomFaveSong":
 				this.restSrvs.getRandomFaveSong(pid)
				.then(data => {

					if (!data['detail']) {
						let track = data['results'][0];
						this.play(track);
					}
				});
 				break;
 			case "getRandomBookmarkedSong":
 				this.restSrvs.getRandomBookmarkedSong(pid)
				.then(data => {
					
					if (!data['detail']) {
						let track = data['results'][0];
						this.play(track);
					}
				});
 				break;
 			case "getRandomTracksOfArtist":
 				this.restSrvs.getRandomTracksOfArtist(pid)
				.then(data => {
					
					if (!data['detail']) {
						let track = data['results'][0];
						this.play(track);
					}
				});
 				break;
 			case "getRandomTrackOfHashtag":
  				let hid = this.playlist.hashtag_id;

 				this.restSrvs.getRandomTrackOfHashtag(hid)
				.then(data => {
					
					if (!data['detail']) {
						let track = data['results'][0];
						this.play(track);
					}
				});
 				break;

 			case "getRandomDownloaded":
				let track = this.playlist.tracks[
					Math.floor(Math.random() * this.playlist.tracks.length)
				];

				this.play(track);
 				break;

 			case "album":
				let track_album = this.playlist.tracks[
					Math.floor(Math.random() * this.playlist.tracks.length)
				];

				this.play(track_album);
 				break;

 			case "search":
				let track_search = this.playlist.tracks[
					Math.floor(Math.random() * this.playlist.tracks.length)
				];

				this.play(track_search);
 				break;

 		}
  	}




  	changeMode() {
  		this.mode++;

  		if (!this.isListplay)
  			this.mode %= 2;
  		else
  			this.mode %= 4;
  	}



	isOFF() {
  		return (this.mode == this.modes.OFF);
  	}


  	isRepeatOnce() {
  		return (this.mode == this.modes.REPEATE_ONCE);
  	}


  	isRepeatList() {
  		return (this.mode == this.modes.REPEATE_LIST);
  	}


  	isRandom() {
  		return (this.mode == this.modes.RANDOM);
  	}


  	icon() {
  		if (this.isRepeatOnce() || this.isRepeatList())
  			return 'repeat';
  		else if (this.isRandom)
  			return 'shuffle';


  		return '';
  	}
}



