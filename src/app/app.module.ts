import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera/ngx';


import { File, FileEntry } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } 
        from '@ionic-native/file-transfer/ngx';

import { Media } from '@ionic-native/media/ngx';
import { MusicControls } from '@ionic-native/music-controls/ngx';

// import { Keyboard } from '@ionic-native/keyboard/ngx';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { AdMobFree } from '@ionic-native/admob-free/ngx';


@NgModule({
  declarations: [
    AppComponent,
    
  ],
  entryComponents: [],
  imports: [BrowserModule, 
  IonicModule.forRoot(),
   AppRoutingModule,
    HttpClientModule,
    ],
  providers: [
    File,
    FileTransfer,
    Camera,
    StatusBar,
    SplashScreen,
    Media,
    MusicControls,
    // Keyboard,
    SocialSharing,
    AdMobFree, 

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
