import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
// import { Router } from '@angular/router';
import { AuthService } from '../auth.service'
import { LoadingController } from '@ionic/angular';
import { NavigatorService } from '../navigator.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	errorMsg = '';
	username = '';
	password = '';

	constructor(
		// private router: Router,
		private navigatorSrvs: NavigatorService,
  		private authSrvs: AuthService,
		private storage: Storage,
		public loadingController: LoadingController) { }

	ngOnInit() {
	}


	async presentLoading() {
		const loading = await this.loadingController.create({
		  	// message: 'جاري تسجيل الدخول',
		  	duration: 2000
		});

		await loading.present();
		const { role, data } = await loading.onDidDismiss();
		
		this.login();
	}


	login() {
		console.log(this.username, this.password);

		let usercredentials = { 
			username: this.username, 
			password: this.password
		};


		this.authSrvs.login(usercredentials).then(token => {
			this.navigatorSrvs.goHome();

			}).catch((e) => {
				console.log("catch from login");
				this.errorMsg = e.error.non_field_errors;
		});

	}



	login_guest() {
		
		this.authSrvs.login_guest().then(token => {
			this.navigatorSrvs.goHome();

			}).catch((e) => {
				console.log("catch from login");
				this.errorMsg = e.error.non_field_errors;
		});		
	}
}
