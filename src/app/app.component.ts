import { Component, Output, EventEmitter } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavigatorService } from './navigator.service';
import { AuthService } from './auth.service';
import { MenuController } from '@ionic/angular';
import { PlayerService  } from './player.service';
import { CachedService } from './cached.service';
import { RestService } from './rest.service';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  @Output() showMyDownloadsEvent = new EventEmitter();


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navigatorSrvs: NavigatorService,
    private authSrvs: AuthService,
    public menuCtrl: MenuController,
    public cachedSrvs: CachedService,
    public restSrvs: RestService,
    public playerSrvs: PlayerService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }



  showMyProfile() {
    this.menuCtrl.close();

    let u = this.cachedSrvs.user_details;

    this.restSrvs.getKasaitUser(u.id).then(data => {

      let map = new Map([
        ['u', data],
        ['o', true]
       ]);

      this.navigatorSrvs.goTo("user-profile", map);
    });
  }



  showMyDownloads() {
    this.menuCtrl.close();
    this.navigatorSrvs.goHome();
    this.showMyDownloadsEvent.emit();
  }


  showDownloaded() {
    this.menuCtrl.close();
    this.navigatorSrvs.goTo("downloads");
  }



  showArtists() {
    this.menuCtrl.close();
    this.navigatorSrvs.goTo("artists");
  }


  showFeedback() {
    this.menuCtrl.close();
    this.navigatorSrvs.goTo("feedback");
  }
  


  showTerms() {
    this.menuCtrl.close();
    this.navigatorSrvs.goTo("terms");
  }


  logout() {

    this.playerSrvs.closeTrack();
    this.playerSrvs.track = null;

    this.authSrvs.logout();
    this.menuCtrl.close();
  }

}
