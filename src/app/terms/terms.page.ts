import { Component, OnInit } from '@angular/core';
import { NavigatorService } from '../navigator.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {


	slideOpts = {
	    initialSlide: 0,
	    scrollbar: true,
	    autoHeight: true,
	    speed: 400
	};

	constructor(
		private nav: NavigatorService,
		) { }

	ngOnInit() {
	}


	skip() {
		this.nav.goHome();
	}
}
