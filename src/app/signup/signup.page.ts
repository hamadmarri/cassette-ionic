import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RestService } from '../rest.service';
import { NavigatorService } from '../navigator.service';
import { FormBuilder, FormGroup, FormControl, Validators  } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {


	base64Image = null;
	message='';

	signupForm: FormGroup;


  	constructor(

  		private camera: Camera,
  		private restSrvs: RestService,
  		private navigatorSrvs: NavigatorService,
  		private formBuilder: FormBuilder,
  		) { } 

  	ngOnInit() {
  		this.signupForm = this.formBuilder.group({
			username: ['', 
					[
						Validators.required,
						Validators.email,
						Validators.minLength(6),
						Validators.maxLength(200),
					]
				],
			password: ['', 
					[
						Validators.required,
						Validators.minLength(6),
						Validators.maxLength(25),
					]
				],
			repassword: ['', 
					[
						Validators.required,
						Validators.minLength(6),
						Validators.maxLength(25),
					]
				],
			firstname: ['', [
						Validators.required,
						Validators.minLength(2),
						Validators.maxLength(50),
					]
				],
			lastname: ['', Validators.maxLength(50)],
			gender: ['0', Validators.required],
			bio: ['', Validators.maxLength(250)],


		});
  	}




	AccessGallery(){
		const options: CameraOptions = {
		  quality: 50,
		  // targetWidth: 100,
  		  targetHeight: 100,
		  destinationType: this.camera.DestinationType.DATA_URL,
		  encodingType: this.camera.EncodingType.JPEG,
		  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
		  mediaType: this.camera.MediaType.PICTURE
		}

		this.camera.getPicture(options).then((imageData) => {
		 // imageData is either a base64 encoded string or a file URI
		 // If it's base64 (DATA_URL):

		 this.base64Image = 'data:image/jpeg;base64,' + imageData;

		}, (err) => {
		 // Handle error
		});
	}


	onSubmit() {

		let p = this.signupForm.get("password").value;
		let p2 = this.signupForm.get("repassword").value;

		if (p != p2) {
			this.signupForm.setErrors({
				'خطا' : 'كلمة السر غير مطابقة'
			});

			this.message = "كلمة السر غير مطابقة";
		} else {
			// if (this.signupForm.get("lastname").value === '') {
			// 	console.log("empty");
			// 	this.signupForm.get("lastname").setValue(' ');
			// }
			this.signup();
		}
	}


	signup() {
		 let avatar = this.dataURItoBlob(this.base64Image);

		 this.restSrvs.signupKasaituser(
		 	this.signupForm.get('username').value,
		 	this.signupForm.get('password').value,
		 	this.signupForm.get('lastname').value,
		 	this.signupForm.get('firstname').value, 
		 	this.signupForm.get('bio').value,
		 	this.signupForm.get('gender').value,
		 	avatar)
		 .then(res => {
		 	// this.navigatorSrvs.goHome();
		 	this.navigatorSrvs.goTo("terms");
		 })

		 .catch(err => {
		 	this.message += JSON.stringify(err);
		 });	
	}


	dataURItoBlob(dataURI) {

		if (!dataURI)
			return null;

	    // convert base64 to raw binary data held in a string
	    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
	    var byteString = atob(dataURI.split(',')[1]);

	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	    // write the bytes of the string to an ArrayBuffer
	    var ab = new ArrayBuffer(byteString.length);
	    var ia = new Uint8Array(ab);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    //New Code
	    return new Blob([ab], {type: mimeString});


	}



	goLogin() {
		this.navigatorSrvs.goTo("login");
	}
	
}
