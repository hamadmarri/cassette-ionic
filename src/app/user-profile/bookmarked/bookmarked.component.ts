import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../player.service';
import { ToastController } from '@ionic/angular';
import { RestService } from '../../rest.service';
import { PlaylistService } from '../../playlist.service'


@Component({
  selector: 'app-bookmarked',
  templateUrl: './bookmarked.component.html',
  styleUrls: ['./bookmarked.component.scss'],
})
export class BookmarkedComponent implements OnInit {

	kasait_user:any = null;
	tracks:any[] = [];
	next:any = null;
	playlistForPlayer;
	loaded = false;


  	constructor(
  		public playerSrvs: PlayerService,
  		public toastController: ToastController,
  		public restSrvs: RestService,
		private playlistSrvs: PlaylistService,
  		) { }

  	ngOnInit() {}


	loadData(kasait_user) {
		this.loaded = false;
		
		if (!this.kasait_user)
			this.kasait_user = kasait_user;

		this.tracks = [];

		this.getTracks();
	}



	getTracks(page = '') {
		this.restSrvs.getBookmarkedSongs(this.kasait_user.id, page)
		.then(data => {
			this.tracks.push(...data['results']);
			this.next = data['next'];
			this.loaded = true;
		});
	}



	more(event) {
		setTimeout(() => {
	  		if (this.next) {
	  			this.getTracks(this.next);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  		
	  	}, 1200);
	}


	playTrack(track) {
		this.playerSrvs.playTrack(track);

		// deep copy
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: this.kasait_user.id,
			tracks: tracks,
			next: this.next,
			moreTracks: 'getBookmarkedSongs',
			random: 'getRandomBookmarkedSong'
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
	}


	remove(t) {
  		this.restSrvs.unbookmarkTrack(t.id)
			.then(res => {

				let index = this.tracks.findIndex((el) => {
					return (el.id === t.id);
				});
				
				this.tracks.splice(index, 1);
				// console.log(this.hashtags);

	  			this.presentToast(t.name + " حذفت من المفضلة");
			});
  	}


  	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}


}
