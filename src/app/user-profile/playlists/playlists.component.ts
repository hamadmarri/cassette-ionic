import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';
import { CachedService } from '../../cached.service';
import { NavigatorService } from '../../navigator.service';
import { PlayerService } from '../../player.service';
import { PlaylistService } from '../../playlist.service'


@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss'],
})
export class PlaylistsComponent implements OnInit {

    kasait_user:any = null;
    isOwner = false;
    playlists:any;
    tracks:[{id:any, tracks: any}] = [,];
    nextPage = null;
    cache_k;
    cache_v;
    playlistForPlayer = null;


  	constructor(
  		public restSrvs: RestService,
  		public cachedSrvs: CachedService,
      public navigatorSrvs: NavigatorService,
  		public playerSrvs: PlayerService,
      private playlistSrvs: PlaylistService,
  		) { }

  	ngOnInit() {
  	}


  	loadData(kasait_user, isOwner) {
  		// if (!this.kasait_user)
  		this.kasait_user = kasait_user;
  		this.isOwner = isOwner;
  		this.cache_k = "playlists:" + this.kasait_user.id;

  		if (!this.cached()) {
	  		this.playlists = [];
	  		this.tracks = [,];
	  		this.nextPage = null;

  			this.getPlaylists();
  		}

  	}


  	cached() {

  		if (!this.cachedSrvs.has(this.cache_k)) {
  			console.log("!cached", this.cache_k);
  			return false;
  		}

  		this.cache_v = this.cachedSrvs.get(this.cache_k);
  		console.log("cached"); //, this.cache_k, this.cache_v);

  		this.playlists = this.cache_v.get('playlists');
  		this.tracks = this.cache_v.get('tracks');
  		this.nextPage = this.cache_v.get('nextPage');

  		return true;
  	}



  	cache() {

  		this.cache_v = new Map([
  			['playlists', this.playlists],
  			['tracks', this.tracks],
  			['nextPage', this.nextPage],
  		]);

  		this.cachedSrvs.cache(this.cache_k, this.cache_v);
  	}


  	getPlaylists(page='') {
  		
		  this.restSrvs.getPlaylistsOf(this.kasait_user.id, page)
			.then(data => {
	  			this.playlists.push(...data['results']);

	  			this.nextPage = data['next'];
	  			// console.log(data);


	  			for (var i = 0; i < data['results'].length; ++i) {
	  				let p = data['results'][i];
	  				
  					this.restSrvs.getTracksOfPlaylist(p.id)
  						.then(data => {
  							this
  							.tracks.push(...[{id:p.id, tracks: data}]);

  							this.cache();

  						});
	  			}

			});
	}



	more(event) {
  		// console.log("more is called");

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.getPlaylists(this.nextPage);
	  			
	  		}
	  		event.target.complete();
	  		
	  	}, 1200);
  	}


  	moreTracks(p) {

  		this.restSrvs.getTracksOfPlaylist(p.id, p.tracks['next'])
			.then(data => {
				p.tracks['results'].push(...data['results']);
				p.tracks['next'] = data['next'];
				// console.log(p);
			});
  	}



	getTracksOf(id) {
		let p = this.tracks.filter(function (el) {
  			return el.id == id;
		});

		if (p.length > 0)
			return p[0];
	}


	showPlaylist(p) {
		let map = new Map([
			['u', this.kasait_user],
			['p', p],
			['o', this.isOwner]
		]);
		this.navigatorSrvs.goTo('playlist', map);
	}


 
  playTrack(p, track) {
    // console.log(track);
    this.playerSrvs.playTrack(track);


    // deep copy 
    let p_tracks = JSON.parse(
      JSON.stringify(this.getTracksOf(p.id))
      );

    this.playlistForPlayer = {
      playlist_id: p.id,
      tracks: p_tracks.tracks['results'],
      next: p_tracks.tracks['next'],
      moreTracks: 'getTracksOfPlaylist',
      random: 'getRandomTracksOfPlaylist'
    };

    this.playlistSrvs.playlist_set(this.playlistForPlayer);
  }

} 
