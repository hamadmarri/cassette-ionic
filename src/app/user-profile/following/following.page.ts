import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';
import { NavigatorService } from '../../navigator.service';

@Component({
  selector: 'app-following',
  templateUrl: './following.page.html',
  styleUrls: ['./following.page.scss'],
})
export class FollowingPage implements OnInit {

	kasait_user:any = null;
	users:any[] = [];
	artists:any[] = [];
	nextArtists:any = null;
	nextUsers:any = null;
	artists_count = -1;
	users_count = -1;
	// updated = false;


	constructor(
		public navigatorSrvs: NavigatorService,
		public restSrvs: RestService,
		) { }



	ngOnInit() { }

 
	ionViewWillEnter() {
		this.kasait_user = this.navigatorSrvs.page_data().get('u');
		this.users = [];
		this.artists = [];
	}


	ionViewDidEnter() {
		this.getArtists();
		this.getUsers();
	}



	getArtists(page = '') {
		this.restSrvs.getFollowingArtists(this.kasait_user.id, page)
		.then(data => {
			this.artists.push(...data['results']);
			this.nextArtists = data['next'];
			this.artists_count = data['count'];
		});
	}


	getUsers(page = '') {
		this.restSrvs.getFollowingUsers(this.kasait_user.id, page)
		.then(data => {
			this.users.push(...data['results']);
			this.nextUsers = data['next'];
			this.users_count = data['count'];
		});
	}



	moreArtists() {
	  	if (this.nextArtists)
	  		this.getArtists(this.nextArtists);
	}


	moreUsers() {
		if (this.nextUsers)
	  		this.getUsers(this.nextUsers);
	}


	showFollowers() {
  		this.navigatorSrvs.goTo("followers",
  			new Map([['u', this.kasait_user]]));
	}


}
 