import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../player.service';
import { PlaylistService } from '../../playlist.service'
import { ToastController } from '@ionic/angular';
import { DownloaderService } from '../../downloader.service';


@Component({
  selector: 'app-downloaded',
  templateUrl: './downloaded.component.html',
  styleUrls: ['./downloaded.component.scss'],
})
export class DownloadedComponent implements OnInit {


	tracks:any[] = [];
	playlistForPlayer = null;


	constructor(
  		public playerSrvs: PlayerService,
		private playlistSrvs: PlaylistService,
  		public toastController: ToastController,
  		public downloaderSrvs: DownloaderService,
		) { }


	ngOnInit() {}


	loadData() {
		this.tracks = [];

		this.getTracks();
	}

	getTracks(page = '') {
		this.tracks = this.downloaderSrvs.downloads;
	}


	playTrack(track) {
		// this.playerSrvs.playTrack(track, track.link);
		this.downloaderSrvs.play(track);

		// deep copy
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: null,
			tracks: tracks,
			next: null,
			moreTracks: null,
			random: 'getRandomDownloaded',
			download: true,
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
	}



	remove(t) {

		let index = this.tracks.findIndex((el) => {
			return (el.id === t.id);
		});
		
		this.tracks.splice(index, 1);
		// console.log(this.hashtags);

		this.downloaderSrvs.remove(t);

		this.presentToast(t.name + " حذفت من قائمة التنزيل");
  	}


  	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
