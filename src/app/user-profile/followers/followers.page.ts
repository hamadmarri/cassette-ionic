import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';
import { NavigatorService } from '../../navigator.service';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.page.html',
  styleUrls: ['./followers.page.scss'],
})
export class FollowersPage implements OnInit {

	kasait_user:any = null;
	users:any[] = [];
	next:any = null;
	users_count = -1;


	constructor(public navigatorSrvs: NavigatorService,
		public restSrvs: RestService,) { }

	ngOnInit() { }



	ionViewWillEnter() {
		this.kasait_user = this.navigatorSrvs.page_data().get('u');
		this.users = [];
	}


  	ionViewDidEnter() {
		this.getUsers();
	}


	getUsers(page = '') {
		this.restSrvs.getFollowers(this.kasait_user.id, page)
		.then(data => {
			this.users.push(...data['results']);
			this.next = data['next'];
			this.users_count = data['count'];
		});
	}



	more(event) {
		console.log("more is called");

		setTimeout(() => {
	  		if (this.next) {
	  			this.getUsers(this.next);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  		
	  	}, 1200);
	}


	showFollowing() {
  		this.navigatorSrvs.goTo("following", 
  			new Map([['u', this.kasait_user]]));
	}
	
}
