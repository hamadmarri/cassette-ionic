import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

 
import { IonicModule } from '@ionic/angular';

import { UserProfilePage } from './user-profile.page';


import { SharedModule } from '../shared/shared.module'
import { PlaylistsComponent } from './playlists/playlists.component'
import { FaveHashtagsComponent } from './fave-hashtags/fave-hashtags.component'
import { FaveSongsComponent } from './fave-songs/fave-songs.component'
import { DownloadedComponent } from './downloaded/downloaded.component'
import { BookmarkedComponent } from './bookmarked/bookmarked.component'


const routes: Routes = [
  {
    path: '',
    component: UserProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    UserProfilePage,
    PlaylistsComponent,
    FaveHashtagsComponent,
    FaveSongsComponent,
    DownloadedComponent,
    BookmarkedComponent,
  ]
})
export class UserProfilePageModule {}
