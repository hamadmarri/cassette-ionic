import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaveHashtagsPage } from './fave-hashtags.page';

describe('FaveHashtagsPage', () => {
  let component: FaveHashtagsPage;
  let fixture: ComponentFixture<FaveHashtagsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaveHashtagsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaveHashtagsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
