import { Component, OnInit } from '@angular/core';
import { RestService } from '../../rest.service';
import { CachedService } from '../../cached.service';
import { NavigatorService } from '../../navigator.service';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-fave-hashtags',
  templateUrl: './fave-hashtags.component.html',
  styleUrls: ['./fave-hashtags.component.scss'],
})
export class FaveHashtagsComponent implements OnInit {

	kasait_user:any = null;
	hashtags:any[] = [];
	next:any = null;
	loaded = false;


	constructor(
		public restSrvs: RestService,
		public navigatorSrvs: NavigatorService,
    	public cachedSrvs: CachedService,
    	public toastController: ToastController,) { }

	ngOnInit() {}

	loadData(kasait_user) {
		this.loaded = false;
  		
  		if (!this.kasait_user)
  			this.kasait_user = kasait_user;

  		// if (this.hashtags)
  		// this.hashtags.length = 0;
  		this.hashtags = [];


  		this.getHashtags();
  	}



  	getHashtags(page = '') {
		this.restSrvs.getFaveHashtags(this.kasait_user.id, page)
		.then(data => {
			this.hashtags.push(...data['results']);
			this.next = data['next'];
			this.loaded = true;
		});
	}



	more(event) {
  		console.log("more is called");

  		setTimeout(() => {
	  		if (this.next) {
	  			this.getHashtags(this.next);
	  			event.target.complete();
	  		} else {
	  			event.target.disabled = true;
	  		}
	  		
	  	}, 1200);
  	}


  	showHashtag(h) {

  		if (h.playlist) {
		  this.restSrvs.getPlaylist(h.playlist)
		  .then(data => {

		    let p = data;

		    this.restSrvs.getKasaitUser(p['kasaituser'])
		    .then(data => {
		      
		      let map = new Map([
		      	['u', data],
		      	['p', p]
		      ]);

		      this.navigatorSrvs.goTo('playlist', map);

		    });
		  });

		 

		} else {
		  this.navigatorSrvs.goTo("hashtag", new Map([['h', h]]));
		}

	  }


  	remove(h) {
  		this.restSrvs.unfaveHashtag(h.id)
			.then(res => {

				let index = this.hashtags.findIndex((el) => {
					return (el.id === h.id);
				});
				
				this.hashtags.splice(index, 1);
				// console.log(this.hashtags);

	  			this.presentToast(h.name + " حذفت من المفضلة");
			});
  	}


  	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
