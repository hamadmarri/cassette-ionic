import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaveSongsPage } from './fave-songs.page';

describe('FaveSongsPage', () => {
  let component: FaveSongsPage;
  let fixture: ComponentFixture<FaveSongsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaveSongsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaveSongsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
