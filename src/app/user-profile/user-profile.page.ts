import { Component, OnInit, ViewChild } from '@angular/core';
import { CachedService } from '../cached.service';
import { RestService } from '../rest.service';
import { PlaylistsComponent } from './playlists/playlists.component'
import { FaveHashtagsComponent } from './fave-hashtags/fave-hashtags.component'
import { FaveSongsComponent } from './fave-songs/fave-songs.component'
import { DownloadedComponent } from './downloaded/downloaded.component'
import { BookmarkedComponent } from './bookmarked/bookmarked.component'
import { ToastController } from '@ionic/angular';
import { NavigatorService } from '../navigator.service';


 enum Pages {
	Playlists,
	FaveHashtags,
	FaveSongs,
	Downloaded,
	Bookmarked,
}


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

	@ViewChild(PlaylistsComponent) playlists;
	@ViewChild(FaveHashtagsComponent) faveHashtags;
	@ViewChild(FaveSongsComponent) faveSongs;
	@ViewChild(DownloadedComponent) downloaded;
	@ViewChild(BookmarkedComponent) bookmarked;


	kasait_user:any = null;
	isOwner = false;
	public pages = Pages;
	page = [true, false, false, false, false];
	// updated = false;
	addingPlaylist = false;
	newPlaylistName = ''; 
	_isSubscribed = -1;

	// sub_kasait_user_changed;

	constructor(
		public cachedSrvs: CachedService,
		public restSrvs: RestService,
		public toastController: ToastController,
    	public navigatorSrvs: NavigatorService,
		) 
	{ 
	}


	ngOnInit() { }


	ionViewWillEnter() {

		this.kasait_user = this.navigatorSrvs.page_data().get('u');
		if (this.navigatorSrvs.page_data().has('o'))
			this.isOwner = this.navigatorSrvs.page_data().get('o');
		else
			this.isOwner = false;

		this._isSubscribed = -1;
	}


	ionViewDidEnter() {
		// default view
		this.playlists.loadData(this.kasait_user, this.isOwner);
		this.setPage(Pages.Playlists, true);
	}



	setPage(page, show_hide: boolean) {
		this.page = [false, false, false, false, false];
		this.page[page] = show_hide;
	}

	ShowPlaylists() {
		this.setPage(Pages.Playlists, true);
		this.playlists.loadData(this.kasait_user, this.isOwner);
	}

	ShowFaveHashtags() {
		this.setPage(Pages.FaveHashtags, true);
		this.faveHashtags.loadData(this.kasait_user);
	}

	ShowFaveSongs() {
		this.setPage(Pages.FaveSongs, true);
		this.faveSongs.loadData(this.kasait_user);
	}


	ShowDownloaded() {
		this.setPage(Pages.Downloaded, true);
		this.downloaded.loadData();
	}

	ShowBookmarked() {
		this.setPage(Pages.Bookmarked, true);
		this.bookmarked.loadData(this.kasait_user);
	}


	showFollowing() {
  		this.navigatorSrvs.goTo("following", 
  			new Map([['u', this.kasait_user]]));
	}

	showFollowers() {
  		this.navigatorSrvs.goTo("followers",
  			new Map([['u', this.kasait_user]]));
	}


	showAddPlaylist() {
		this.addingPlaylist = true;	
	}



	addPlaylist() {

		this.restSrvs.createPlaylist(this.newPlaylistName)
		.then(res => {
			console.log(res);
			
			this.playlists.loadData(this.kasait_user);
			this.presentToast("تم اضافة " + this.newPlaylistName);
			this.newPlaylistName = '';
			this.addingPlaylist = false;
		});
	}


	isSubscribed() {

		if (this._isSubscribed > -1) {
			// console.log("_isSubscribed");

			return (this._isSubscribed === 1);
		}

		// console.log("isSubscribed");
		let index = this.cachedSrvs.following_users
					.findIndex(el => {
						return el === this.kasait_user.id
					});

		if (index > -1)
			this._isSubscribed = 1;
		else
			this._isSubscribed = 0;

		return (this._isSubscribed === 1);
	}


	subscribe() {
		this.restSrvs.subscribeKasaitUser(this.kasait_user.id)
		.then(res => {
			
			this.cachedSrvs.cacheFollowings();
			this._isSubscribed = 1;
			this.kasait_user.followers_count++;

			this.presentToast("تمت متابعة " 
				+ this.kasait_user.user.first_name);
		});
	}


	unsubscribe() {
		this.restSrvs.unsubscribeKasaitUser(this.kasait_user.id)
		.then(res => {
			
			this.cachedSrvs.cacheFollowings();
			this._isSubscribed = 0;
			this.kasait_user.followers_count--;
			
			this.presentToast("تم الغاء متابعة " 
				+ this.kasait_user.user.first_name);
		});
	}


	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
