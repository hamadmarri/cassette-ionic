import { Component, OnInit } from '@angular/core';
import { CachedService } from '../cached.service';
import { NavigatorService } from '../navigator.service';
import { RestService } from '../rest.service';
import { PlayerService } from '../player.service';
import { PlaylistService } from '../playlist.service'


@Component({
  selector: 'app-album',
  templateUrl: './album.page.html',
  styleUrls: ['./album.page.scss'],
})
export class AlbumPage implements OnInit {

	album = null;
	tracks:any[] = [];
	nextTracks = null;
	playlistForPlayer = null;

	constructor(
		public cachedSrvs: CachedService,
		public navigatorSrvs: NavigatorService,
		private restSrvs: RestService,
    	public playerSrvs: PlayerService,
		private playlistSrvs: PlaylistService,
		) { }

	ngOnInit() { }


	ionViewWillEnter() {
		// console.log("ionViewWillEnter");

		this.album = this.navigatorSrvs.page_data().get('a');

		this.tracks = [];
		this.getTracks();
	}


	getTracks(page='') {
		// let a = this.cachedSrvs.album_get();

		this.restSrvs.getTracksOfAlbum(this.album.id, page)
		.then(data => {
			this.tracks.push(...data['results']);
			this.nextTracks = data['next'];
		});
	}


	playTrack(track) {
		// console.log(track);
		this.playerSrvs.playTrack(track);


		// deep copy 
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: 0,
			tracks: tracks,
			next: null,
			moreTracks: '',
			random: 'album'
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
	}

	
}
