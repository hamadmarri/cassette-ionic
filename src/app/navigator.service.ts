import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { CachedService } from './cached.service';

import { NavController } from '@ionic/angular'

export interface PageContext {
	name: string;
	data: Map<string, any>; 
}



@Injectable({
  providedIn: 'root'
})
export class NavigatorService {


	@Output() showSearch = new EventEmitter();


	pages: PageContext[] = [];


	constructor(
		private router: Router,
		private navCtrl: NavController,
		private cachedSrvs: CachedService,
		) 
	{
		let page = {name: "home", data: null}
		this.pages.push(page);
	}


	public goTo(page_name, page_data=null) {

		let page = {name: page_name, data: page_data}
		this.pages.push(page);

		// console.log("push", this.pages);

		// this.router.navigate([page.name]);
		this.navCtrl.navigateForward(page.name);
	}
 

	public back() {
		this.pages.pop();
		// console.log("pop", this.pages);

		// this.router.navigate([this.currentPage().name]);
		this.navCtrl.navigateBack(this.currentPage().name);
	}
 

	public currentPage() {
		return this.pages[this.pages.length - 1];
	}


	public page_data() {
		return this.currentPage().data;
	}


	public goHome(showSearch = false) {
		this.pages = [];

		let page = {name: "home", data: null}
		this.pages.push(page);

		// this.cachedSrvs.clear();

		// this.router.navigate(['home']);
		this.navCtrl.navigateRoot('home');

		if (showSearch) {
			this.showSearch.emit();
		}
	}
} 
