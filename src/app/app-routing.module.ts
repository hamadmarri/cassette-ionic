import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage'; 
import { AuthService } from './auth.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthService] },
  { path: 'user-profile', loadChildren: './user-profile/user-profile.module#UserProfilePageModule' },
  { path: 'max-player', loadChildren: './max-player/max-player.module#MaxPlayerPageModule' },
  { path: 'hashtag', loadChildren: './hashtag/hashtag.module#HashtagPageModule' },
  { path: 'following', loadChildren: './user-profile/following/following.module#FollowingPageModule' },
  { path: 'followers', loadChildren: './user-profile/followers/followers.module#FollowersPageModule' },
  { path: 'playlist', loadChildren: './playlist/playlist.module#PlaylistPageModule' },
  { path: 'add-to-playlist', loadChildren: './playlist/add-to-playlist/add-to-playlist.module#AddToPlaylistPageModule' },
  { path: 'artist', loadChildren: './artist/artist.module#ArtistPageModule' },
  { path: 'album', loadChildren: './album/album.module#AlbumPageModule' },
  { path: 'downloads', loadChildren: './downloads/downloads.module#DownloadsPageModule' },
  { path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackPageModule' },
  { path: 'artists', loadChildren: './artists/artists.module#ArtistsPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
];

@NgModule({
  imports: 
  [
  	RouterModule.forRoot(routes),
  	IonicStorageModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
