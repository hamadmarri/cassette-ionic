import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage'
import { RestService } from './rest.service'

@Injectable({
  providedIn: 'root'
})
export class CachedService {


  public user_details: any;
  public following_users: number[] = [];
  public following_artists: number[] = [];
  public caches = new Map<string, any>();


  constructor(
    public storage: Storage,
    public restSrvs: RestService,
  ) {

     // get user details from storage then cache it
    this.storage.get('userdetails').then(u => {
      this.user_details_set(u);


      if (this.user_details) {
        // cache followings
        this.cacheFollowings();
      }
      
      
    });

  } 



  public has(k) {
    // console.log("has", this.caches);
    return this.caches.has(k);
  }


  public get(k) {
    return this.caches.get(k);
  }


  public cache(k, v) {
    // console.log("caching", k, v);

    this.caches.set(k, v); 
  }


  public delete(k) {
    this.caches.delete(k);
  }
  

  public clear() {
    this.caches.clear();
  }


  public user_details_set(u) {
    this.user_details = u;
  }



  public cacheFollowings() {

    setTimeout(() => {
      // check if tooken is ready
      if (this.restSrvs.token === '') {
        // if not, try again (500ms between checks)
        this.cacheFollowings();
      }
      else {
        // otherwise do caching
        this.following_artists = [];
        this.following_users = [];

        this.cacheFollowingArtists();
        this.cacheFollowingUsers();

      }
    }, 500);
  }


  public cacheFollowingArtists(page='') {
    this.restSrvs.getFollowingArtists(this.user_details.id, page)
    .then(data => {
      let mapped = data['results'].map(a => (a.id));
      this.following_artists.push(...mapped);
      if(data['next'])
        this.cacheFollowingArtists(data['next']);
    });
  }


  public cacheFollowingUsers(page='') {
    this.restSrvs.getFollowingUsers(this.user_details.id, page)
    .then(data => {
      let mapped = data['results'].map(a => (a.id));
      this.following_users.push(...mapped);
      if(data['next'])
        this.cacheFollowingUsers(data['next']);
    });
  
  }


}
