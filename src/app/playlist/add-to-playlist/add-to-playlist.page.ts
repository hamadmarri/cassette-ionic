import { Component, OnInit } from '@angular/core';
import { CachedService } from '../../cached.service';
import { RestService } from '../../rest.service';
import { ToastController } from '@ionic/angular';
import { NavigatorService } from '../../navigator.service';


@Component({
  selector: 'app-add-to-playlist',
  templateUrl: './add-to-playlist.page.html',
  styleUrls: ['./add-to-playlist.page.scss'],
})
export class AddToPlaylistPage implements OnInit {


	kasait_user = null;
	// isOwner = false;
	playlists:any[] = [];
	track = null;
	addingPlaylist = false;
	newPlaylistName = '';

	constructor(
		public cachedSrvs: CachedService,
		public restSrvs: RestService,
		public toastController: ToastController,
		public navigatorSrvs: NavigatorService,
		) { }

	ngOnInit() {
		// this.kasait_user = this.cachedSrvs.kasait_user_get();
		// this.track = this.cachedSrvs.track;

		this.kasait_user = this.navigatorSrvs.page_data().get('u');
		this.track = this.navigatorSrvs.page_data().get('t');

		// if (this.navigatorSrvs.page_data().length === 3){
		// 	this.isOwner = this.navigatorSrvs.page_data()[2];
		// }
		// else{
		// 	this.isOwner = false;
		// }

		this.getPlaylists();
	}


	getPlaylists(page='') {
		this.restSrvs.getPlaylistsOf(this.kasait_user.id, page)
		.then(data => {
			this.playlists.push(...data['results']);
	  		if (data['next']) {
	  			this.getPlaylists(data['next']);
	  		}
		});
	}


	addToPlaylist(p) {

		this.restSrvs.addToPlaylist(p.id, this.track.id)
		.then(res => {
			let cache_k = "playlists:" + this.kasait_user.id;
			this.cachedSrvs.delete(cache_k);

			this.presentToast("تم اضافة " + this.track.name + " الى " + p.name);
			this.navigatorSrvs.back();
		});
	}


	showAddPlaylist() {
		this.addingPlaylist = true;	
	}



	addPlaylist() {

		this.restSrvs.createPlaylist(this.newPlaylistName)
		.then(res => {
			
			this.playlists = [];
			this.getPlaylists();
						
			this.presentToast("تم اضافة " + this.newPlaylistName);
			this.newPlaylistName = '';
			this.addingPlaylist = false;
		});
	}

	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
