import { Component, OnInit } from '@angular/core';
import { CachedService } from '../cached.service';
import { RestService } from '../rest.service';
import { PlayerService } from '../player.service';
import { ToastController } from '@ionic/angular';
import { NavigatorService } from '../navigator.service';
import { PlaylistService } from '../playlist.service'


@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.page.html',
  styleUrls: ['./playlist.page.scss'],
})
export class PlaylistPage implements OnInit {

	kasait_user;
	playlist;
	tracks:any[] = [];
	nextPage = null;
	isOwner = false;
	playlistForPlayer = null;
	tracks_count = -1;


	constructor(
		public cachedSrvs:CachedService,
		public restSrvs:RestService,
    	public playerSrvs: PlayerService,
		public toastController: ToastController,
		public navigatorSrvs: NavigatorService,
		private playlistSrvs: PlaylistService,
		) 
	{

	}


	ngOnInit() { }

	
	ionViewWillEnter() {
		this.kasait_user = this.navigatorSrvs.page_data().get('u');
		this.playlist = this.navigatorSrvs.page_data().get('p');

		if (this.navigatorSrvs.page_data().has('o')){
			this.isOwner = this.navigatorSrvs.page_data().get('o');
		}
		else{
			this.isOwner = false;
		}
	}

	ionViewDidEnter() {
		this.tracks = [];
		this.getTracks();
	}



	getTracks(page='') {
		// console.log(this.playlist.name);
		this.restSrvs.getTracksOfPlaylist(this.playlist.id, page)
		.then(data => {
			this.tracks.push(...data['results']);
	  		this.nextPage = data['next'];
			this.tracks_count = data['count'];
	  		
		});
	}


	more(event) {

  		setTimeout(() => {
	  		if (this.nextPage) {
	  			this.getTracks(this.nextPage);
	  			
	  		}
	  		event.target.complete();
	  		
	  	}, 1200);
  	}


	playTrack(track) {
		// console.log(track);
		this.playerSrvs.playTrack(track);


		// deep copy 
		let tracks = JSON.parse(JSON.stringify(this.tracks));

		this.playlistForPlayer = {
			playlist_id: this.playlist.id,
			tracks: tracks,
			next: this.nextPage,
			moreTracks: 'getTracksOfPlaylist',
			random: 'getRandomTracksOfPlaylist'
		};

		this.playlistSrvs.playlist_set(this.playlistForPlayer);
	}


	remove(t) {
  		this.restSrvs.removeFromPlaylist(this.playlist.id, t.id)
			.then(res => {

				let index = this.tracks.findIndex((el) => {
					return (el.id === t.id);
				});
				
				this.tracks.splice(index, 1);
				// console.log(this.hashtags);

	  			this.presentToast(t.name + " حذفت من القائمة");

	  			// this.cachedSrvs.update(this.cachedSrvs.kasait_user_get());
			});
  	}


  	delete() {
  		this.restSrvs.deletePlaylist(this.playlist.id)
  		.then(res => {
	  		this.presentToast("تم حذف " + this.playlist.name);
	  		// this.cachedSrvs.update(this.cachedSrvs.kasait_user_get());
		    this.navigatorSrvs.back();
  		});
  	}
 


 	showUserProfile() {
		this.navigatorSrvs.goTo("user-profile", new Map([['u', this.kasait_user]]));
	}


  	showFollowing() {
  		this.navigatorSrvs.goTo("following", 
  			new Map([['u', this.kasait_user]]));
	}

	showFollowers() {
  		this.navigatorSrvs.goTo("followers", 
  			new Map([['u', this.kasait_user]]));
	}


	faveHashtag() {
		this.restSrvs.faveHashtag(this.playlist.hashtag)
			.then(res => {
	  			this.presentToast(this.playlist.name + " اضيفت الى المفضلة");
			});
	}


  	async presentToast(message) {
		const toast = await this.toastController.create({
			message: message,
			duration: 2000
		});
		toast.present();
	}
}
